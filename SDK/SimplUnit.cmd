:: ==================================================================
:: Copyright (c) 2016 Crestron EMEA
::
:: Permission is hereby granted, free of charge, to any person
:: obtaining a copy of this software and associated documentation
:: files (the "Software"), to deal in the Software without
:: restriction, including without limitation the rights to use,
:: copy, modify, merge, publish, distribute, sublicense, and/or sell
:: copies of the Software, and to permit persons to whom the
:: Software is furnished to do so, subject to the following
:: conditions:
::
:: The above copyright notice and this permission notice shall be
:: included in all copies or substantial portions of the Software.
::
:: THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
:: EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
:: OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
:: NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
:: HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
:: WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
:: FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
:: OTHER DEALINGS IN THE SOFTWARE.
:: ==================================================================
::
:: Invokes SUnit.ps1 with variables
::
:: This script only exist for easy execution. You ofcourse could call the 
:: SUnit.ps1 script directly from an suitable environment 
@echo off
SETLOCAL ENABLEDELAYEDEXPANSION

SET sdkPath=%~dp0 

SET binDir=%1
SET targetName=%2
:: The device to run the test like it is know on the network.
:: this may also be it hostname
SET device="10.32.4.2"
:: SET device="10.32.4.154"
:: You can specify more paramters by adding them to the command below
:: -or- you could update the default values in the SUnit.ps1 script
SET simplAppId=10

:: pass parameter 3 till 9 (used for calling this script with parameters)
powershell.exe -command "& { . '%sdkpath%\SimplUnit.ps1'  -binDir '%binDir%' -testDll %targetName% -device %device% -SimplAppId %simplAppId% %3 %4 %5 %6 %7 %8 %9 }"