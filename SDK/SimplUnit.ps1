###  ==================================================================
###  Copyright (c) 2016 Crestron EMEA
### 
###  Permission is hereby granted, free of charge, to any person
###  obtaining a copy of this software and associated documentation
###  files (the "Software"), to deal in the Software without
###  restriction, including without limitation the rights to use,
###  copy, modify, merge, publish, distribute, sublicense, and/or sell
###  copies of the Software, and to permit persons to whom the
###  Software is furnished to do so, subject to the following
###  conditions:
### 
###  The above copyright notice and this permission notice shall be
###  included in all copies or substantial portions of the Software.
### 
###  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
###  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
###  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
###  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
###  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
###  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
###  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
###  OTHER DEALINGS IN THE SOFTWARE.
###  ==================================================================
param (
   [string]$device = "",
   [string]$SimplAppId = "10",
   [string]$username = "crestron",
   [string]$password = "",
   [string]$binDir = "",
   [string]$testDll = "",
   [string]$remoteDir = "\Nvram\SimplUnit",
   [string]$SDKPath = $PSScriptRoot,
   [switch]$deploySDK = $false,
   [switch]$noReset = $false,
   [switch]$undeploySDK = $false
)

Set-ExecutionPolicy -Scope CurrentUser RemoteSigned
Import-Module PSCrestron

$filenames = Get-ChildItem -Path $binDir -Recurse -Include *.dll
$testDll = "$testDll.dll"
$reset = !$noReset

echo ""
echo "  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo "    Copyright (c) 2016 Crestron EMEA "
echo "    SimplUnit testing "
echo "    Device: $device" 
echo "  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo ""
if($deploySDK){
    echo "   Uploading SimplUnit test harness..."
    $localAppClz = "$SDKPath\Crestron.EMEA.SimplUnit.ConsoleRunner.cpz"
    Send-CrestronProgram -Device $device -LocalFile $localAppClz -Secure -Username $username -Password $password -ProgramSlot $SimplAppId
}
$session = Open-CrestronSession -Device $device -Secure -Username $username -Password $password
$remoteSessionDir = "$remoteDir\$session"
Invoke-CrestronSession -Handle $session -Command "makedir $remoteSessionDir"
ForEach($f in $filenames){
    $filename = [System.IO.Path]::GetFileName($f)
    $remoteFile = "$remoteSessionDir\$filename"
    echo "   Uploading $filename..."
    Send-FTPFile -Device "$device" -LocalFile "$f" -RemoteFile "$remoteFile"
}
echo ""
echo "   Invoking SimplUnit for $testDll"
$testResult = Invoke-CrestronSession -Handle $session -Command "SimplUnit:$SimplAppId $remoteSessionDir\$testDll"
echo ""

if($undeploySDK){
	echo "   Stopping the testprogram..."
	$null = Invoke-CrestronSession -Handle $session -Command "stopprog -p:$SimplAppId"
	echo "   Unregistering the testprogram..."
	$null = Invoke-CrestronSession -Handle $session -Command "progreg -p:$SimplAppId -u"
}

if($reset){    
    $null = Invoke-CrestronSession -Handle $session -Command "progreset -p:$SimplAppId"
}


if($undeploySDK -Or $reset){
	# Cleaning the session folder is only possible when the test harness is not running. 
	echo "   Cleaning the session folder..."
	$null = Invoke-CrestronSession -Handle $session -Command "del $remoteSessionDir\*"
	$null = Invoke-CrestronSession -Handle $session -Command "removedir $remoteSessionDir"
}


Close-CrestronSession -Handle $session

echo ""
echo "   ~~~~~~~~~~~~~~~~~~~~~~~~~~ TEST RESULT ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo $testResult
echo "   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
