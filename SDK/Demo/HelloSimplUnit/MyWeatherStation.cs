﻿namespace HelloSimplUnit
{
    public interface MyWeatherStation
    {
        bool IsConnected();
        int Temp { get; }
        string WindDirection { get; }

        void GetCurrentTemp();
        void GetCurrentWindDirection();
    }
}