﻿namespace HelloSimplUnit.SillyTemp
{
    public interface WeatherStation
    {
        void Initialize();
        string Send(string request);        
    }
}