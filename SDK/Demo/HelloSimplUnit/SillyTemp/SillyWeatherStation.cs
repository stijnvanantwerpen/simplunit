﻿using System;
using System.Linq;

namespace HelloSimplUnit.SillyTemp
{
    public class SillyWeatherStation : WeatherStation
    {
        readonly Random _rnd = new Random();

        public void Initialize()
        {
            #region Set up a connection
            #endregion
        }

        public string Send(string request)
        {
            #region Send request to real device and get a response
            //This is just a dummy implementation to simulate responses from a real device
            if (request == "INIT") return "";
            var temp = _rnd.Next(50);
            var direction = _rnd.Next(360);
            var time = DateTime.Now.ToShortTimeString();
            
            if (request.Count(c => c == ':') != 2) throw new Exception("not a valid request");
            if (request.Count(c => c == ';') != 1) throw new Exception("not a valid request");
            if (request.Split(':')[0] != time.Split(':')[0]) throw new Exception("request out of sync");
            if (request.Split(':')[1] != time.Split(':')[1]) throw new Exception("request out of sync");
            if (request.Split(';')[1] == "T") return String.Format("{0};{1}", time, temp);
            if (request.Split(';')[1] == "D") return String.Format("{0};{1}", time, direction);
            throw new Exception("Invalid type request");
            #endregion
        }
    }
}