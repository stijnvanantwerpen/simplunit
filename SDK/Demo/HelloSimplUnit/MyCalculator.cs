﻿namespace HelloSimplUnit
{
    public class MyCalculator
    {
        public int Add(int x, int y)
        {             
            checked
            {
                //Next line conatins an error
                //return x*y;            
                return x + y;
            }
        }     
    }
}
