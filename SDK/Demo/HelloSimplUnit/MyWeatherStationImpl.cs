﻿using System;
using HelloSimplUnit.SillyTemp;

namespace HelloSimplUnit
{
    public class MyWeatherStationImpl : MyWeatherStation
    {
        private bool _hasBeenInitialized;
        private readonly WeatherStation _weatherStation;

        public MyWeatherStationImpl(WeatherStation weatherStation)
        {
            _weatherStation = weatherStation;
        }

        public bool IsConnected()
        {
            return _weatherStation.Send("INIT") == "CONNECTED";
        }

        public int Temp { get; private set; }
        public string WindDirection { get; private set; }

        public void GetCurrentTemp()
        {
            //Remove the next line, TheStationShouldBeInitialized_1 will fail
            InitializeOnFirstRequest();
            var time = DateTime.Now.ToString("HH:mm:ss");
            var request = String.Format("{0};T", time);
            var response = _weatherStation.Send(request);    
            if(response == null) return;
            var tempAsString = response.Split(';')[1];
            Temp = Int32.Parse(tempAsString)  ;
        }

        public void GetCurrentWindDirection()
        {
            //Remove the next line, TheStationShouldBeInitialized_2 will fail
            InitializeOnFirstRequest();            
        }

        private void InitializeOnFirstRequest()
        {
            //Remove the next line, TheStationShouldBeInitializedOnlyOnce will fail 
            if(_hasBeenInitialized)return;

            _weatherStation.Initialize();
            _hasBeenInitialized = true;
        }
    }
}