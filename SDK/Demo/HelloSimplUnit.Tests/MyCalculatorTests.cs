﻿using Crestron.EMEA.SimplUnit;

namespace HelloSimplUnit.Tests
{
    public class MyCalculatorTests
    {
        [Fact] 
        public void TwoAndTwoShouldBeFour()
        {            
            var sut = new MyCalculator();
            var result = sut.Add(2, 2);
            result.ShouldBe(4);
        }

        [Fact(Skip = "Reason to skip")]
        public void ThreeAndFourShouldBeSeven()
        {
            var sut = new MyCalculator();
            var result = sut.Add(3, 4);
            result.ShouldBe(7);           
        }

        [Theory]
        [InlineData(2, 2, 4)]
        [InlineData(3, 4, 7)]
        [InlineData(0, 0, 0)]
        public void TestAdd(int x, int y, int expected)
        {
            var sut = new MyCalculator();
            var result = sut.Add(x, y);
            result.ShouldBe(expected);            
        }

        [Fact]
        public void EndOfBoundry()
        {
            var sut = new MyCalculator();            
            sut.ShoudThrowAnException(myCalculator => 
                myCalculator.Add(int.MaxValue, 1));
        }       
    }
}
