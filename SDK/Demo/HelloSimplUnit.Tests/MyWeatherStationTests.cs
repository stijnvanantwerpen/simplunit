﻿using Crestron.EMEA.SimplUnit;
using HelloSimplUnit.Tests.Mock;

namespace HelloSimplUnit.Tests
{
    public class MyWeatherStationTests
    {
        [Fact]
        public void TheStationShouldBeInitialized_1()
        {
            //Arrange
            var mockWeatherStation = new WeatherStationMock();
            mockWeatherStation.Expect(e => e.Initialize()).Once();
            mockWeatherStation.Expect(e => e.Initialize()).Never();

            //Act
            var sut = new MyWeatherStationImpl(mockWeatherStation);
            sut.GetCurrentTemp();

            //Assert
            mockWeatherStation.VerifyAllExpectations();
        }
        [Fact]
        public void TheStationShouldBeInitialized_2()
        {
            //Arrange
            var mockWeatherStation = new WeatherStationMock();
            mockWeatherStation.Expect(e => e.Initialize()).Once();

            //Act
            var sut = new MyWeatherStationImpl(mockWeatherStation);
            sut.GetCurrentWindDirection();

            //Assert
            mockWeatherStation.VerifyAllExpectations();
        }
        [Fact]
        public void TheStationShouldBeInitializedOnlyOnce_1()
        {
            //Arrange
            var mockWeatherStation = new WeatherStationMock();
            mockWeatherStation.Expect(e => e.Initialize()).Once();

            //Act
            var sut = new MyWeatherStationImpl(mockWeatherStation);
            sut.GetCurrentTemp();
            sut.GetCurrentTemp();
            sut.GetCurrentWindDirection();

            //Assert
            mockWeatherStation.VerifyAllExpectations();
        }

        [Fact]
        public void TheStationShouldBeInitializedOnlyOnce_2()
        {
            //Arrange
            var mockWeatherStation = new WeatherStationMock();
            mockWeatherStation.Expect(e => e.Initialize()).Once();

            //Act
            var sut = new MyWeatherStationImpl(mockWeatherStation);
            sut.GetCurrentWindDirection();
            sut.GetCurrentTemp();
            sut.GetCurrentTemp();

            //Assert
            mockWeatherStation.VerifyAllExpectations();
        }

        [Fact]
        public void ARequestShouldContainTheCurrentTime()
        {
            //Arrange
            var mockWeatherStation = new WeatherStationMock();
            mockWeatherStation.Expect(e => 
                e.Send(Arg<string>.Match(
                    request => request.Length > 4
                        && request[2] == ':'))).Once();

            //Act
            var sut = new MyWeatherStationImpl(mockWeatherStation);
            sut.GetCurrentTemp();

            //Assert
            mockWeatherStation.VerifyAllExpectations();
        }

        [Fact]
        public void TestForGetCurrentTemperature()
        {
            var mockWeatherStation = new WeatherStationMock();
            mockWeatherStation.Expect(e => e.Send(Arg<string>.Match(r => r.Split(';')[1] == "T")))
                .Return("00:00:00;23");

            var sut = new MyWeatherStationImpl(mockWeatherStation);
            sut.GetCurrentTemp();
            var result = sut.Temp;

            result.ShouldBe(23);
            mockWeatherStation.VerifyAllExpectations();
        }

        [Fact]
        public void TestForIsConnected_True()
        {
            //Arrange
            var mockWeatherStation = new WeatherStationMock();
            mockWeatherStation.Expect(e => e.Send("INIT")).Return("CONNECTED");
            
            //Act
            var sut = new MyWeatherStationImpl(mockWeatherStation);
            var result = sut.IsConnected();

            //Assert
            result.ShouldBe(true);
            mockWeatherStation.VerifyAllExpectations();
        }

        [Fact]
        public void TestForIsConnected_False()
        {
            //Arrange
            var mockWeatherStation = new WeatherStationMock();
            mockWeatherStation.Expect(e => e.Send("INIT")).Return("");

            //Act
            var sut = new MyWeatherStationImpl(mockWeatherStation);
            var result = sut.IsConnected();

            //Assert
            result.ShouldBe(false);
            mockWeatherStation.VerifyAllExpectations();
        }

        [Fact]
        public void TestWithAnyValue()
        {           
            var mockWeatherStation = new WeatherStationMock();
            mockWeatherStation.Expect(e => e.Send(Arg<string>.Any)).Return("Hello World").Always();

            mockWeatherStation.Send("").ShouldBe("Hello World");
            mockWeatherStation.Send("ABC").ShouldBe("Hello World");
            mockWeatherStation.Send("XYZ").ShouldBe("Hello World");
            mockWeatherStation.Send(null).ShouldBe("Hello World");
        }
    }
}