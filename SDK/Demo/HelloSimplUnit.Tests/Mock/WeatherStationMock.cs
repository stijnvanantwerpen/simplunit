﻿using Crestron.EMEA.SimplUnit;
using Crestron.EMEA.SimplUnit.SimplMock;
using HelloSimplUnit.SillyTemp;

namespace HelloSimplUnit.Tests.Mock
{
    public class WeatherStationMock: BaseMock<WeatherStationInvoker>, WeatherStation
    {
        public WeatherStationMock() : base(new WeatherStationInvoker())
        {
        }

        public void Initialize()
        {
            Do(i => i.Initialize());
        }

        public string Send(string request)
        {
            return Do(i => i.Send(request));
        }
    }
}