﻿using Crestron.EMEA.SimplUnit;
using Crestron.EMEA.SimplUnit.SimplMock;

namespace HelloSimplUnit.Tests.Mock
{
    public class WeatherStationInvoker : Invoker
    {
        public Expectation<string> Send(Arg<string> request)
        {
            return GetExpectationFor<string>("Send", request);
        }

        public ExpectationVoid Initialize()
        {
            return GetExpectationFor("Initialize");
        }
    }
}