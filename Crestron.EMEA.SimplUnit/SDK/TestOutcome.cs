﻿namespace Crestron.EMEA.SimplUnit.SDK
{
    public enum TestOutcome
    {
        Succes,
        Skipped,
        Failed
    }
}