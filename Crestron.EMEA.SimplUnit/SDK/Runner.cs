﻿using System;
using Crestron.EMEA.SimplUnit.SDK.Imp;

namespace Crestron.EMEA.SimplUnit.SDK
{
    public interface Runner
    {
        void Add(Test test);
        void Run();
        event EventHandler<TestExecutedEventArgs> OnTestExecuted;
    }
}