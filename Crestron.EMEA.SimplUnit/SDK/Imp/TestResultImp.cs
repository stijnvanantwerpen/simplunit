﻿namespace Crestron.EMEA.SimplUnit.SDK.Imp
{
    public class TestResultImp : TestResult
    {
        public TestOutcome TestOutcome { get; set; }
        public string Message { get; set; }        
    }
}