﻿// Copyright (c) 2016 Crestron EMEA
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
using System;
using System.Collections.Generic;

namespace Crestron.EMEA.SimplUnit.SDK.Imp
{
    public class TestSuiteImp : TestSuite
    {
        private readonly List<Test> _tests = new List<Test>();

        public TestSuiteImp(string description)
        {
            Description = description;
        }

        public string Description { get; private set; }

        public event EventHandler<TestSuiteEventArgs> OnExecutionBegin;
        public event EventHandler<TestExecutedEventArgs> OnTestExecuted;
        public event EventHandler<TestSuiteEventArgs> OnExecutionEnd;

        public void Execute()
        {
            BeginTests();
            DoTests();
            EndTests();            
        }

        private void BeginTests()
        {
            InvokeOnExecutionBegin(new TestSuiteEventArgs { TestSuite = this });
        }
        private void DoTests()
        {
            foreach (var test in _tests)
            {
                test.OnTestExecuted += OnTestExecuted;
                test.Execute();
                test.OnTestExecuted -= OnTestExecuted;
            }
        }
        private void EndTests()
        {
            InvokeOnExecutionEnd(new TestSuiteEventArgs {TestSuite = this});
        }

        public void Add(Test test)
        {
            _tests.Add(test);
        }

        public void Add(IEnumerable<Test> tests)
        {
            _tests.AddRange(tests);            
        }

        protected virtual void InvokeOnExecutionBegin(TestSuiteEventArgs e)
        {
            if (OnExecutionBegin == null) return;
            OnExecutionBegin(this, e);
        }

        protected virtual void InvokeOnExecutionEnd(TestSuiteEventArgs e)
        {
            if (OnExecutionEnd == null) return;
            OnExecutionEnd(this, e);
        }
    }
}