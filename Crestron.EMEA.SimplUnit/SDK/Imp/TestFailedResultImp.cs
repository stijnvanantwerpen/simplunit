﻿namespace Crestron.EMEA.SimplUnit.SDK.Imp
{
    public class TestFailedResultImp : TestResultImp
    {
        public TestFailedResultImp()
        {
            TestOutcome = TestOutcome.Failed;
        }

        public string StackTrace { get; set; }
    }
}