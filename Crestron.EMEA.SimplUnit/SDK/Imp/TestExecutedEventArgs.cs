﻿using System;

namespace Crestron.EMEA.SimplUnit.SDK.Imp
{
    public class TestExecutedEventArgs : EventArgs
    {
        public Test Test { get; set; }
        public TestResult Result { get; set; }
    }
}