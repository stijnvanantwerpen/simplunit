﻿using System;
using Crestron.SimplSharp.Reflection;

namespace Crestron.EMEA.SimplUnit.SDK.Imp
{
    public class TestCaseImp : TestCase
    {
        public string Description { get; private set; }

        public event EventHandler<TestExecutedEventArgs> OnTestExecuted;

        private readonly Action _testMethode;               
        private string _skip;
        private bool _shouldBeSkipped;

        public string Skip
        {
            get
            {
                return _skip;
            }
            set
            {
                _skip = value;
                _shouldBeSkipped = true;
            }
        }

        public TestCaseImp(Action testMethode, string description)
        {
            Description = description;
            _testMethode = testMethode;
        }

        public void Execute()
        {
            if (_shouldBeSkipped)
            {
                InvokeOnTestExecuted_AsSkipped();
            }
            else
            {
                Execute_NoneSkippedTest();
            }
        }

        private void Execute_NoneSkippedTest()
        {
            try
            {
                _testMethode();
                InvokeOnTestExecuted_AsSucces();
            }
            catch (TargetInvocationException ex)
            {
                InvokeOnTestExecuted_AsFailedTest(ex);
            }
            catch (Exception ex)
            {
                InokveOntestExecuted_AsExecutionOfTestFailed(ex);
            }
        }

        private void InokveOntestExecuted_AsExecutionOfTestFailed(Exception ex)
        {
            InvokeOnTestExecuted(new TestExecutedEventArgs
            {
                Test = this,
                Result = new TestResultImp
                {
                    TestOutcome = TestOutcome.Failed,
                    Message = ex.Message
                }
            });
        }

        private void InvokeOnTestExecuted_AsFailedTest(TargetInvocationException ex)
        {
            InvokeOnTestExecuted(new TestExecutedEventArgs
            {
                Test = this,                
                Result = new TestFailedResultImp
                {                                        
                    Message = ex.InnerException.Message,
                    StackTrace = ex.InnerException.StackTrace
                }
            });
        }

        private void InvokeOnTestExecuted_AsSucces()
        {
            InvokeOnTestExecuted(new TestExecutedEventArgs
            {
                Test = this,
                Result = new TestResultImp
                {
                    TestOutcome = TestOutcome.Succes
                }
            });
        }

        private void InvokeOnTestExecuted_AsSkipped()
        {
            InvokeOnTestExecuted(new TestExecutedEventArgs
            {
                Test = this,
                Result = new TestResultImp
                {
                    TestOutcome = TestOutcome.Skipped,
                    Message = Skip
                }
            });
        }

        protected virtual void InvokeOnTestExecuted(TestExecutedEventArgs args)
        {
            if (OnTestExecuted == null) return;
            OnTestExecuted(this, args);
        }
    }
}