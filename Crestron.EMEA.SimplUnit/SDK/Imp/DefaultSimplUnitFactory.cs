﻿using System;

namespace Crestron.EMEA.SimplUnit.SDK.Imp
{
    public class DefaultSimplUnitFactory : SimplUnitFactory
    {
        public virtual Runner CreateRunner()
        {
            return new RunnerImp();
        }

        public TestSuite CreateTestSuite(string description)
        {
            return new TestSuiteImp(description);
        }

        public TestCase CreateTestCase(Action action, string description)
        {
            return new TestCaseImp(action, description);
        }
    }
}