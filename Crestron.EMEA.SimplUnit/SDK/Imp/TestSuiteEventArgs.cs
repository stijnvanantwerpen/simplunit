﻿using System;

namespace Crestron.EMEA.SimplUnit.SDK.Imp
{
    public class TestSuiteEventArgs : EventArgs
    {
        public TestSuite TestSuite { get; set; }
    }
}