﻿using System;

namespace Crestron.EMEA.SimplUnit.SDK
{
    public interface SimplUnitFactory
    {
        Runner CreateRunner();
        TestSuite CreateTestSuite(string description);
        TestCase CreateTestCase(Action action, string description);
    }
}