﻿using System;
using System.Collections.Generic;
using Crestron.EMEA.SimplUnit.SDK.Imp;

namespace Crestron.EMEA.SimplUnit.SDK
{
    public interface TestSuite : Test
    {
        void Add(Test test);
        void Add(IEnumerable<Test> tests);
        event EventHandler<TestSuiteEventArgs> OnExecutionBegin;
        event EventHandler<TestSuiteEventArgs> OnExecutionEnd;
    }
}