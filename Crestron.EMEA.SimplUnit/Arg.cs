// Copyright (c) 2016 Crestron EMEA
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.

using System;
using System.Collections;
using System.Linq;
using Crestron.EMEA.SimplUnit.Tools;

namespace Crestron.EMEA.SimplUnit
{  
    public class Arg<T> 
    {
        private readonly T _value;
        private readonly bool _isAny;

        private readonly Func<T, bool> _matchFunc;
        private readonly bool _isMatch;

        public Arg(T value)
        {
            _value = value;
            _isAny = false;
        }

        protected Arg()
        {
            _isAny = true;
        }

        private Arg(Func<T, bool> func)
        {
            _isMatch = true;
            _matchFunc = func;
        }

        public static Arg<T> Any
        {
            get { return new Arg<T>(); }
        }

        public T Value
        {
            get { return _value; }            
        }

        public override int GetHashCode()
        {
            return _isAny ? 0 : _value.GetHashCode();
        }

        protected bool Equals(Arg<T> other)
        {
            return _isAny || _value.Equals(other);
        }

        // ReSharper disable once CompareNonConstrainedGenericWithNull        
        public override bool Equals(object other)
        {
            if(_isAny) return true;
            if (!IsOfCompatibleType(other)) return false;
            if (_isMatch) return IsMatch(other);
            return !AreBothIEnumerable(other) ? other.Equals(_value) : AreSequenceEqual(other);
        }

        private bool AreSequenceEqual(object other)
        {
            var valueAsIEnum = (_value as IEnumerable).Cast<object>();
            var otherAsIEnum = (other as IEnumerable).Cast<object>();
            return valueAsIEnum.SequenceEqual(otherAsIEnum, new SequenceEqualComparer());
        }

        private bool AreBothIEnumerable(object other)
        {
            return (_value is IEnumerable) && (other is IEnumerable);
        }

        private bool IsMatch(object other)
        {
            if (other is T) return _matchFunc((T) other);
            return false;
        }

        private static bool IsOfCompatibleType(object other)
        {
            return (other is T || other is Arg<T>);
        }

        public override string ToString()
        {
            return String.Format("Arg<{0}>.{1}", typeof (T).Name, 
                _isAny ? "Any" : _isMatch ? "Match" :  _value.ToString());
        }

        public static implicit operator Arg<T>(T value)
        {
            return new Arg<T>(value);
        }

        public static implicit operator T(Arg<T> arg)
        {
            if (arg._isAny) throw new SimplUnitException("'Arg<T>.Any' can not be cast to a value.");
            if (arg._isMatch) throw new SimplUnitException("'Arg<T>.Match' can not be cast to a value.");

            return arg.Value;
        }

        public static Arg<T> Match(Func<T, bool> func)
        {
            return new Arg<T>(func);
        }
    }
}