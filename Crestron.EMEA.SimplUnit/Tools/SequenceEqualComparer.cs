using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Crestron.EMEA.SimplUnit.Tools
{
    public class SequenceEqualComparer : IEqualityComparer<object>
    {
        public new bool Equals(object x, object y)
        {
            var xAsIEnum = (x is IEnumerable)?(x as IEnumerable).Cast<object>():null;
            var yAsIEnum = (y is IEnumerable)?(y as IEnumerable).Cast<object>():null;
            if (xAsIEnum == null && yAsIEnum == null) return x.Equals(y);
            return xAsIEnum != null && xAsIEnum.SequenceEqual(yAsIEnum, this);
        }

        public int GetHashCode(object obj)
        {
            var objAsIEnum = (obj is IEnumerable) ? (obj as IEnumerable).Cast<object>() : null;
            return objAsIEnum == null ? obj.GetHashCode() : objAsIEnum.Sum(o => GetHashCode(o));
        }
    }
}