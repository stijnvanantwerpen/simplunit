// Copyright (c) 2016 Crestron EMEA
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.

using System.Linq;

namespace Crestron.EMEA.SimplUnit.Tools
{
    public class ArgComparer<T> : FuncEqualityComparer<Arg<T>>
    {
        public ArgComparer()
            : base((arg1, arg2) =>
            {
                var arr1 = arg1.Value as Arg<object>[];
                var arr2 = arg2.Value as Arg<object>[];
                if (arr1 == null && arr2 == null)
                {
                    var objArr1 = arg1.Value as object[];
                    var objArr2 = arg2.Value as object[];
                    if(objArr1 == null && objArr2 == null)return arg1.Equals(arg2);
                    if (objArr1 == null || objArr2 == null) return false;
                    if(objArr1.GetType() != objArr2.GetType()) return false;
                    return objArr1.SequenceEqual(objArr2, new SequenceEqualComparer());
                } 
                if (arr1 == null || arr2 == null) return false;
                return arr1.SequenceEqual(arr2, new ArgComparer<object>());
            })
        {
        }      
    }
}