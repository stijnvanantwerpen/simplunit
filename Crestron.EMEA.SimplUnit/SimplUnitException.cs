using System;

namespace Crestron.EMEA.SimplUnit
{
    public class SimplUnitException : Exception
    {
        public SimplUnitException(string msg, params object[] args):base(String.Format(msg, args))
        {            
        }
    }
}