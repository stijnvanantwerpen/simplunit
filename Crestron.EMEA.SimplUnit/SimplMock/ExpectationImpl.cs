﻿namespace Crestron.EMEA.SimplUnit.SimplMock
{
    public class ExpectationImpl<TResult> : ExpectationBase, Expectation<TResult>
    {

        public ExpectationImpl(IInvoker parrentInvoker, string methodeName, Arg<object>[] args)
            : base(parrentInvoker, methodeName, args)
        {
        }

        public Expectation<TResult> AddOnce(TResult result)
        {            
            ResultQueue.Add(new ReturnSetOnce<TResult>(result, this));
            return this;
        }

        public TResult GetResult(bool strict)
        {            
            return  ResultQueue.GetNextResultSet<TResult>().GetResult(strict);
        }

        public ExpectationBuilder<TResult> GetBuilder()
        {
            return new ExpectationBuilder<TResult>(this);   
        }

        public void AddAlways(TResult result)
        {
            ResultQueue.Add(new ReturnSetAlways<TResult>(result, this));
        }

        public void AddNever(TResult result)
        {
            ResultQueue.Add(new ReturnSetNever<TResult>(result, this));
        }
     
    }
}