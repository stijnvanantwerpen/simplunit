namespace Crestron.EMEA.SimplUnit.SimplMock
{
    public interface Expectation<TResult> : Expectation
    {
        Expectation<TResult> AddOnce(TResult result);
        TResult GetResult(bool strict);
        ExpectationBuilder<TResult> GetBuilder();
        void AddAlways(TResult result);
        void AddNever(TResult result);
    }

    public interface Expectation
    {    
        int GetHashCode();
        bool EvaluateSignature(string methodeName, params Arg<object>[] args);
        void Verify();
        bool Equals(object o);
        void AddAlways();
        void AddOnce();
        string MethodeName { get; }
        string Description { get; }
    }
}