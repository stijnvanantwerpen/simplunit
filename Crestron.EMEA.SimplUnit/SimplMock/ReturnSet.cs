namespace Crestron.EMEA.SimplUnit.SimplMock
{
    public abstract class ReturnSet
    {
        protected readonly Expectation Parrent;
        protected bool WasCalled;

        protected ReturnSet(Expectation parrent)
        {
            Parrent = parrent;
        }

        public virtual bool IsOnce()
        {
            return false;
        }

        public virtual void RegisterCall(bool strict)
        {
            WasCalled = true;
        }

        public virtual void Verify()
        {            
        }
    }

    public class ReturnSetDefault : ReturnSet
    {
        public ReturnSetDefault(Expectation parrent) : base(parrent)
        {
        }

        public override void RegisterCall(bool strict)
        {
            if(strict)throw new SimplUnitException("All expected calls has been made on", Parrent.MethodeName );
        }
    }

    public class ReturnSetOnce : ReturnSet
    {
        public ReturnSetOnce(Expectation parrent) : base(parrent)
        {
        }

        public override bool IsOnce()
        {
            return true;
        }

        public override void Verify()
        {
            if (WasCalled) return;
            throw new SimplUnitException("Expected call on {0} was never executed", Parrent.Description);
        }
    }

    public class ReturnSetAlways : ReturnSet
    {
        public ReturnSetAlways(Expectation parrent): base(parrent)
        {
            
        }
    }

    public class ReturnSetNever : ReturnSet
    {
        public ReturnSetNever(Expectation parrent) : base(parrent)
        {
        }

        public override void Verify()
        {
            if(!WasCalled)return;
            throw new SimplUnitException("A Call on {0} was not expected", Parrent.Description);
        }
    }

    public abstract class ReturnSet<TResult> : ReturnSet
    {
        private readonly TResult _result;   

        protected ReturnSet(TResult result, Expectation parrent) : base(parrent)
        {
            _result = result;
        }

        public TResult GetResult(bool strict)
        {
            RegisterCall(strict);
            return _result;
        }
    }

    public class ReturnSetDefault<TResult> : ReturnSet<TResult>
    {
        public ReturnSetDefault(Expectation parrent) : base(default(TResult), parrent)
        {
        }

        public override void RegisterCall(bool strict)
        {
            if (strict) throw new SimplUnitException("All expected calls on {0} has been made", Parrent.Description);
        }
    }

    public class ReturnSetOnce<TResult> : ReturnSet<TResult>
    {
        public ReturnSetOnce(TResult result, Expectation parrent) : base(result, parrent)
        {
        }

        public override bool IsOnce()
        {
            return true;
        }

        public override void Verify()
        {
            if (WasCalled) return;
            throw new SimplUnitException("a Call on {0} was expected, but no call was actually done", Parrent.Description);
        }
    }

    public class ReturnSetAlways<TResult> : ReturnSet<TResult>
    {
        public ReturnSetAlways(TResult result, Expectation parrent) : base(result, parrent)
        {
        }
    }

    public class ReturnSetNever<TResult> : ReturnSet<TResult>
    {
        public ReturnSetNever(TResult result, Expectation parrent)
            : base(result, parrent)
        {
        }

        public override void Verify()
        {
            if (!WasCalled) return;
            throw new SimplUnitException("Call on {} was not expected", Parrent.Description);
        }
    }
}