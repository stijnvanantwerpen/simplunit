﻿// Copyright (c) 2016 Crestron EMEA
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.

using System;
using System.Linq;
using Crestron.EMEA.SimplUnit.Tools;

namespace Crestron.EMEA.SimplUnit.SimplMock
{
    public abstract class ExpectationBase : Expectation
    {
        protected ExpectationBase(IInvoker parrentInvoker, string methodeName, params Arg<object>[] args)
        {
            MethodeName = methodeName;
            ParrentInvoker = parrentInvoker;
            Parameters = args;
            ResultQueue = new ReturnSetFifoList(this);
        }

        protected bool Equals(ExpectationBase other)
        {
            return string.Equals(MethodeName, other.MethodeName) && Equals(Parameters, other.Parameters);
        }

        public override int GetHashCode()
        {
            return ((MethodeName != null ? MethodeName.GetHashCode() : 0)*397) ^
                   (Parameters != null ? Parameters.GetHashCode() : 0);
        }

        public string MethodeName { get; private set; }

        public string Description
        {
            get { return String.Format("{0}.{1}", 
                ParrentInvoker.Mock.GetType().Name, 
                MethodeName); }
        }

        protected readonly IInvoker ParrentInvoker;
        protected readonly Arg<object>[] Parameters;

        protected readonly ReturnSetFifoList ResultQueue;

        // ReSharper disable once RedundantTypeArgumentsOfMethod
        // ReSharper gets it, S# compiler does not :(
        public bool EvaluateSignature(string methodeName, params Arg<object>[] args)
        {
            if (!MethodeName.Equals(methodeName)) return false;
            if (args == null && Parameters == null) return true;
            if (args == null || Parameters == null) return false;                      
            return args.SequenceEqual<Arg<object>>(Parameters, new ArgComparer<object>());            
        }

        public void Verify()
        {                    
            foreach (var result in ResultQueue)
            {
                result.Verify();
            }            
        }
    
        public override bool Equals(object o)
        {
            var other = o as ExpectationBase;
            return other != null && other.EvaluateSignature(MethodeName, Parameters);
        }

        public void AddAlways()
        {
            ResultQueue.Add(new ReturnSetAlways(this));
        }

        public void AddOnce()
        {
            ResultQueue.Add(new ReturnSetOnce(this));
        }
    }
}