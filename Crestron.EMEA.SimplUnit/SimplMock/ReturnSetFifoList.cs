﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Crestron.EMEA.SimplUnit.SimplMock
{
    public class ReturnSetFifoList : IEnumerable<ReturnSet>
    {
        private readonly Expectation _parrentExpectation;
        private readonly List<ReturnSet> _list = new List<ReturnSet>();

        public ReturnSetFifoList(Expectation parrentExpectation)
        {
            _parrentExpectation = parrentExpectation;
        }

        public void Add(ReturnSet returnSet)
        {
            _list.Add(returnSet);
        }

        public ReturnSet GetNextResultSet()
        {
            var first = _list.FirstOrDefault() ?? new ReturnSetDefault(_parrentExpectation);
            if (first.IsOnce())
            {
                _list.Remove(first);
            }
            return first;
        }

        public ReturnSet<TResult> GetNextResultSet<TResult>()
        {
            return GetNextResultSet() as ReturnSet<TResult> ?? new ReturnSetDefault<TResult>(_parrentExpectation);
        }


        public IEnumerator<ReturnSet> GetEnumerator()
        {
            return _list.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}