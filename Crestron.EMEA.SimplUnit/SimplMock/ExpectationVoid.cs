﻿namespace Crestron.EMEA.SimplUnit.SimplMock
{
    public class ExpectationVoid : ExpectationBase
    {
        public ExpectationVoid(IInvoker parrentInvoker, string methodeName, params Arg<object>[] args) : base(parrentInvoker, methodeName, args)
        {
        }

        internal void RegisterCall(bool strict)
        {
            ResultQueue.GetNextResultSet().RegisterCall(strict);
        }

        public ExpectationBuilder GetBuilder()
        {
            return new ExpectationBuilder(this);
        }

        public void AddNever()
        {
            ResultQueue.Add(new ReturnSetNever(this));
        }
    }
}