namespace Crestron.EMEA.SimplUnit.SimplMock
{
    public class ExpectationBuilder<TResult>
    {
        private TResult _result;

        private readonly Expectation<TResult> _expectation;
        private bool _addedWithDefaultOccurence;

        public ExpectationBuilder(Expectation<TResult> expectation)
        {
            _expectation = expectation;
        }

        public ExpectationBuilder<TResult> Return(TResult result)
        {
            _result = result;
            _expectation.AddOnce(_result);
            _addedWithDefaultOccurence = true;
            return this;
        }

        public void Repeat(int times)
        {            
            if(times < 1) throw new SimplUnitException("Repeat(times={0}) is not allowd, times has to be strict positive");
            for(var i = 0; i < times; i++)Once();
        }

        public void Once()
        {
            if (_addedWithDefaultOccurence)
            {
                _addedWithDefaultOccurence = false;
                return;
            }
            _expectation.AddOnce(_result);
        }

        public void Never()
        {
            _expectation.AddNever(_result);
        }

        public void Always()
        {           
            _expectation.AddAlways(_result);
        }

        public void AtLeastOnce()
        {
            _expectation.AddOnce(_result);
            _expectation.AddAlways(_result);
        }
    }

    public class ExpectationBuilder
    {
        private readonly ExpectationVoid _expectation;
    
        public ExpectationBuilder(ExpectationVoid expectation)
        {
            _expectation = expectation;
        }

        public void Once()
        {
            _expectation.AddOnce();
        }

        public void Never()
        {
            _expectation.AddNever();
        }

        public void Always()
        {
            _expectation.AddAlways();
        }

        public virtual void Repeat(int times)
        {            
            if(times < 1) throw new SimplUnitException("Repeat(times={0}) is not allowd, times has to be strict positive");
            for(var i = 0; i < times; i++)Once();
        }
        
        public void AtLeastOnce()
        {
            _expectation.AddOnce();
            _expectation.AddAlways();
        }
    }
}