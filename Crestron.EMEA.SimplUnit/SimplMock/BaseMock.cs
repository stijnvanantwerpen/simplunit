﻿// Copyright (c) 2016 Crestron EMEA
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.


using System;
using Crestron.SimplSharp.Reflection;
using Activator = Crestron.SimplSharp.Reflection.Activator;

namespace Crestron.EMEA.SimplUnit.SimplMock
{
    public abstract class BaseMock<TInvoker> where TInvoker : Invoker, new()
    {
        protected readonly TInvoker Invoker;
        private bool _isStrict;

        [Obsolete("Use paramterless constructor")]
        protected BaseMock(TInvoker invoker)
        {
            Invoker = invoker;            
        }

        protected BaseMock()
        {
            Invoker = Activator.CreateInstance<TInvoker>();
            Invoker.Mock = this;
        }

        public ExpectationBuilder<TResult> Expect<TResult>(Func<TInvoker, Expectation<TResult>> func)
        {
            return func(Invoker).GetBuilder();
        }

        public ExpectationBuilder Expect(Func<TInvoker, ExpectationVoid> func)
        {
            return func(Invoker).GetBuilder();
        }

        public TResult Do<TResult>(Func<TInvoker, Expectation<TResult>> func)
        {
            return func(Invoker).GetResult(_isStrict);
        }

        public void Do(Func<TInvoker, ExpectationVoid> func)
        {
            func(Invoker).RegisterCall(_isStrict);
        }

        public void VerifyAllExpectations()
        {
            Invoker.VerifyAllExpectations();
        }

        public void SetStrict()
        {
            _isStrict = true;
        }

        public void Trigger<TArg1>(Func<TInvoker, EventInvoker<TArg1>> func, TArg1 arg1)
        {
            func(Invoker).Invoke(arg1);
        }

        public void Trigger<TArg1, TArg2>(Func<TInvoker, EventInvoker<TArg1, TArg2>> func, TArg1 arg1, TArg2 arg2)
        {
            func(Invoker).Invoke(arg1, arg2);
        }

        public void Trigger<TArg1, TArg2, TArg3>(Func<TInvoker, EventInvoker<TArg1, TArg2, TArg3>> func, TArg1 arg1, TArg2 arg2, TArg3 arg3)
        {
            func(Invoker).Invoke(arg1, arg2, arg3);
        }

        public void Trigger(Func<TInvoker, EventInvoker> func)            
        {
            func(Invoker).Invoke();
        }

        protected EventInvoker<TEventArgs> GetEvent<TEventArgs>(EventHandler<TEventArgs> myEvent)
            where TEventArgs : EventArgs
        {
            return new EventInvoker<TEventArgs>
            {
                Invoke = (e) =>
                {
                    if (myEvent == null) return;
                    myEvent(this, e);
                }
            };
        }

        protected EventInvoker GetEvent(EventHandler myEvent)        
        {
            return new EventInvoker
            {
                Invoke = () =>
                {
                    if (myEvent == null) return;
                    myEvent(this, EventArgs.Empty);
                }
            };
        }

        protected EventInvoker<TArg1> GetEvent<TArg1>(Action<TArg1> myEvent)            
        {
            return new EventInvoker<TArg1>
            {
                Invoke = (arg1) =>
                {
                    if (myEvent == null) return;
                    myEvent(arg1);                    
                }
            };
        }

        protected EventInvoker<TArg1, TArg2> GetEvent<TArg1, TArg2>(Action<TArg1, TArg2> myEvent)
        {
            return new EventInvoker<TArg1, TArg2>
            {
                Invoke = (arg1, arg2) =>
                {
                    if (myEvent == null) return;
                    myEvent(arg1, arg2);
                }
            };
        }

        protected EventInvoker<TArg1, TArg2, TArg3> GetEvent<TArg1, TArg2, TArg3>(Action<TArg1, TArg2, TArg3> myEvent)
        {
            return new EventInvoker<TArg1, TArg2, TArg3>
            {
                Invoke = (arg1, arg2, arg3) =>
                {
                    if (myEvent == null) return;
                    myEvent(arg1, arg2, arg3);
                }
            };
        }
    }
}

