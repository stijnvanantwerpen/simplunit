﻿using System;

namespace Crestron.EMEA.SimplUnit.SimplMock
{    
    public class EventInvoker        
    {
        public Action Invoke { get; set; }
    }

    public class EventInvoker<TArg1>      
    {
        public Action<TArg1> Invoke { get; set; }
    }

    public class EventInvoker<TArg1, TArg2>
    {
        public Action<TArg1 , TArg2> Invoke { get; set; }
    }

    public class EventInvoker<TArg1, TArg2, TArg3>
    {
        public Action<TArg1, TArg2, TArg3> Invoke { get; set; }
    }
}