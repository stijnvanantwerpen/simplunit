using System.Collections.Generic;
using System.Linq;

namespace Crestron.EMEA.SimplUnit.SimplMock
{
    public abstract class Invoker : IInvoker
    {
        public object Mock { get; set; }

        readonly List<ExpectationBase> _expectations = new List<ExpectationBase>();        

        private Expectation<T> GetExpectation<T>(string methodeName, Arg<object>[] args)
        {
            return _expectations.FirstOrDefault(e => e.EvaluateSignature(methodeName, args)) as Expectation<T>
                   ?? AddNewExpectation<T>(methodeName, args);
        }

        private Expectation<T> AddNewExpectation<T>(string methodeName, Arg<object>[] args)
        {
            var expectation = new ExpectationImpl<T> (this, methodeName, args);
            _expectations.Add(expectation);
            return expectation;
        }

        private ExpectationVoid GetExpectation(string methodeName, Arg<object>[] args)
        {
            return (_expectations.FirstOrDefault(e => e.EvaluateSignature(methodeName, args))
                    ?? AddNewExpectation(methodeName, args)) as ExpectationVoid;
        }

        private ExpectationBase AddNewExpectation(string methodeName, Arg<object>[] args)
        {
            var expectation = new ExpectationVoid(this, methodeName, args);            
            _expectations.Add(expectation);
            return expectation;
        }

        public Expectation<TReturn> GetExpectationFor<TReturn>(string methodName, params Arg<object>[] args)
        {            
            return GetExpectation<TReturn>(methodName, args);
        }

        public ExpectationVoid GetExpectationFor(string methodName, params Arg<object>[] args)
        {           
            return GetExpectation(methodName, args);
        }

        public void VerifyAllExpectations()
        {
            _expectations.ForEach(e => e.Verify());
        }    
    }
}