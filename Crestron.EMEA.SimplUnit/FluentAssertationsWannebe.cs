﻿#region Licence
// Copyright (c) 2016 Crestron EMEA
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
#endregion
using System;
using System.Collections.Generic;

namespace Crestron.EMEA.SimplUnit
{
    // ReSharper disable CompareNonConstrainedGenericWithNull
    public static class FluentAssertationsWannebe
    {
        public static void ShouldBe(this ushort self, int expected)
        {
            self.ShouldBe((ushort)expected);
        }

        public static void ShouldBe(this byte self, ushort expected)
        {
            self.ShouldBe((byte)expected);
        }

        public static void ShouldBe<TResult>(this Func<TResult> self, TResult expected)
        {
            self().ShouldBe(expected);
        }

        public static void ShouldNotBe<T>(this T self, T expected)
        {
            if (self == null && expected == null) throw new FluentAssertationsException<T>("Value should not be null");
            if (self == null || expected == null) return; 
            if (self.Equals(expected)) throw new FluentAssertationsException<T>("Value should not be {0}", self);
        }

        public static void ShouldBe<T>(this T self, T expected)
        {
            if (self == null && expected == null) return;

            if (self == null || !self.Equals(expected))                 
                throw new FluentAssertationsException<T>(expected, self);
        }
        
        public static void ShouldBeBetween<T>(this T self, T lowerBorder, T upperBorder) where T : IComparable
        {
            self.ShouldBeLower(upperBorder);
            self.ShouldBeHigher(lowerBorder);
        }

        public static void ShouldBeLower<T>(this T self, T upperBorder) where T : IComparable
        {
            if (self.CompareTo(upperBorder) > 0)
                throw new FluentAssertationsException<T>("{0} should be lower than {1}.", self, upperBorder);
        }

        public static void ShouldBeHigher<T>(this T self, T lowerBorder) where T : IComparable
        {
            if (self.CompareTo(lowerBorder) < 0)
                throw new FluentAssertationsException<T>("{0} should be higher than {1}.", self, lowerBorder);
        }

        public static void ShouldBe<T>(this T[] self, T[] expected) {
            if (ReferenceEquals(self, expected))
                return;

            if (self == null || expected == null)
                throw new FluentAssertationsException<T>("Both are not null");

            if (self.Length != expected.Length)
                throw new FluentAssertationsException<T>("Arrays do not have same length.");

            var comparer = EqualityComparer<T>.Default;
            for (var i = 0; i < self.Length; i++) {
                if (!comparer.Equals(self[i], expected[i]))
                    throw new FluentAssertationsException<T>(self[i], expected[i]);
            }            
        }

        public static void ShouldBeNull<T>(this T self)
        {
            if (self != null) throw new FluentAssertationsException<T>("Object was not null while it should be null.");
        }

        public static void ShouldNotBeNull<T>(this T self)
        {
            if (self == null) throw new FluentAssertationsException<T>("Object was null while it should not be null.");
        }

        public static void ShoudThrowAnException(this Action action)
        {
            try
            {
                action();
            }
            catch
            {
                //This is fine, it was expected the invocation would throw an exception
                return;
            }
            throw new FluentAssertationsException<Action>("Expected exception was not thrown");
        }

        public static void ShoudThrowAnException<TResult>(this Func<TResult> func)
        {
            ShoudThrowAnException(((Action)(() => func())));
        }

        public static void ShoudThrowAnException<T>(this T self, Action<T> action)
        {
            ((Action)(() => action(self))).ShoudThrowAnException();
        }

        public static Func<TResult> Invoking<T, TResult>(this T self, Func<T, TResult> func)
        {
            return () => func(self);
        }
        public static Action Invoking<T>(this T self, Action<T> action)
        {
            return () => action(self);
        }
    }

    public class FluentAssertationsException<T> : Exception
    {
        public FluentAssertationsException(T expected, T actual):
            base(String.Format("Expected value was {0}, but {1} was found!", expected, actual))
        {            
        }

        public FluentAssertationsException(string msg):
            base(msg)
        {            
        }

        public FluentAssertationsException(string msg, params object[] args):
            base(String.Format(msg, args))
        {            
        }
        // ReSharper restore CompareNonConstrainedGenericWithNull
    }
}
