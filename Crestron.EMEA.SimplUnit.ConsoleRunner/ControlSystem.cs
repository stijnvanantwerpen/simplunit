// Copyright (c) 2016 Crestron EMEA
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
using System;
using Crestron.EMEA.SimplUnit.SDK.Imp;
using Crestron.SimplSharp;                          	
using Crestron.SimplSharp.Reflection;
using Crestron.SimplSharpPro;                       	
using Crestron.SimplSharpPro.CrestronThread;        	

namespace Crestron.EMEA.SimplUnit.ConsoleRunner
{
    public class ControlSystem : CrestronControlSystem
    {        
        public ControlSystem()           
        {
            try
            {
                Thread.MaxNumberOfUserThreads = 20;
                AddConsoleCommands();
            }
            catch (Exception e)
            {
                ErrorLog.Error("Error in the constructor: {0}", e.Message);
            }
        }

        private static void AddConsoleCommands()
        {
            CrestronConsole.AddNewConsoleCommand(OnConsoleCommandSUnit, "SimplUnit", "SimplUnit Testing",ConsoleAccessLevelEnum.AccessOperator);
        }

        private static void OnConsoleCommandSUnit(string cmdparameters)
        {
            try
            {
                //var runner = new SimplUnitConsoleRunner();                
                var runner = new SimplUnitConsoleRunner(new DefaultSimplUnitFactory());
                var assembly = Assembly.LoadFrom(cmdparameters);
                runner.AddAssembly(assembly);
                runner.Run();
            
            }
            catch (TargetInvocationException ex)
            {
                CrestronConsole.PrintLine("Target Invocation Exception: " + ex.InnerException.Message);
            }
            catch (Exception ex)
            {
                CrestronConsole.PrintLine("{0}: {1}",ex.GetType().Name, ex.Message);
            }
        }    
    }
}