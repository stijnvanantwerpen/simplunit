﻿using System;
using System.Collections.Generic;
using System.Linq;
using Crestron.EMEA.SimplUnit.SDK;
using Crestron.EMEA.SimplUnit.SDK.Imp;
using Crestron.SimplSharp;
using Crestron.SimplSharp.Reflection;

namespace Crestron.EMEA.SimplUnit.ConsoleRunner
{
    public class SimplUnitConsoleRunner
    {
        private readonly SimplUnitFactory _factory;
        private readonly Runner _runner;

        public SimplUnitConsoleRunner(SimplUnitFactory factory)
        {
            _factory = factory;
            _runner = _factory.CreateRunner();
        }

        public void AddAssembly(Assembly assembly)
        {
            var testSuite = CreateTestSuiteForAssembly(assembly);
            _runner.Add(testSuite);
        }

        private Test CreateTestSuiteForAssembly(Assembly assembly)
        {
            var testSuite = CreateTestSuite(assembly);            
            testSuite.Add(
                GetTypesContainingFacts(assembly)
                .Select(type => CreateTestSuite(type) as Test));
            return testSuite;
        }

        private TestSuite CreateTestSuite(Type type)
        {
            var testSuite = _factory.CreateTestSuite(type.FullName);
            testSuite.OnExecutionBegin += (sender, arg) => CrestronConsole.PrintLine("{1}{0}{2}", arg.TestSuite.Description, White, ResetColor);            

            var facts = GetFacts(type);
            foreach (var fact in facts)
            {
                testSuite.Add(CreateTestsForFact(fact, type));
            }
            return testSuite;
        }

        private IEnumerable<Test> CreateTestsForFact(MethodInfo fact, CType type)
        {
            var factAttribute = fact
                .GetCustomAttributes(typeof (FactAttribute), true)
                .Select(attr => (FactAttribute)attr)
                .Single();

            var dataAtributes = fact
             .GetCustomAttributes(typeof(DataAttribute), true)
             .DefaultIfEmpty(new NullDataAttribute())
             .Select(attr => (DataAttribute)attr);

            foreach (var dataAtribute in dataAtributes)
            {
                if (factAttribute.ShouldBeSkipped())
                {
                    var testCase = _factory.CreateTestCase(() => { }, fact.Name);
                    testCase.Skip = factAttribute.Skip;
                    yield return testCase;
                }
                else if (dataAtribute.ShouldBeSkipped())
                {
                    var testCase = _factory.CreateTestCase(() => { },fact.Name + dataAtribute.GetDescription());
                    testCase.Skip = dataAtribute.Skip;                    
                    yield return testCase;
                }
                else
                {
                    var instanceClass = type.GetConstructor(new CType[] { }).Invoke(new object[] { });
                    var attrData = dataAtribute.GetData(fact);
                    foreach (var args in attrData)
                    {
                        var args1 = args;
                        yield return new TestCaseImp(() => fact.Invoke(instanceClass, args1),fact.Name + dataAtribute.GetDescription());
                    }
                }
            }
        }

        private static IEnumerable<MethodInfo> GetFacts(CType type)
        {
            return type.GetMethods().Where(m => m.GetCustomAttributes(typeof(FactAttribute), true).Any());
        }

        private TestSuite CreateTestSuite(Assembly assembly)
        {
            return _factory.CreateTestSuite(assembly.GetName().Name);                        
        }

        private static IEnumerable<CType> GetTypesContainingFacts(Assembly assembly)
        {
            return assembly.GetTypes()
                .Where(t =>
                    //GetMethods() not supported for Open Generic Types
                    !t.IsGenericType 
                    && t.GetMethods().Any(m => 
                        m.GetCustomAttributes(typeof(FactAttribute), true).Any()));
        }

        public void Run()
        {
            _runner.OnTestExecuted += HandleOnTestExecuted();
            _runner.Run();
        }

        private static EventHandler<TestExecutedEventArgs> HandleOnTestExecuted()
        {
            return (sender, args) =>
            {
                switch (args.Result.TestOutcome)
                {
                    case TestOutcome.Succes:
                        CrestronConsole.PrintLine("  ({0}V{1}) {2}"
                            , Green, ResetColor
                            , args.Test.Description);
                        break;
                    case TestOutcome.Skipped:
                        CrestronConsole.PrintLine("  ({0}V{1}) {2}: {3}"
                            , Yellow, ResetColor
                            , args.Test.Description
                            , args.Result.Message);
                        break;
                    case TestOutcome.Failed:
                        CrestronConsole.PrintLine("  ({0}X{1}) {2}: {3}"
                            , Red, ResetColor
                            , args.Test.Description
                            , args.Result.Message);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
                   
            };
        }

        public const string Red = "\x1B[31;1m";
        public const string Green = "\x1B[32;1m";
        public const string Yellow = "\x1B[33;1m";
        public const string Blue = "\x1B[34;1m";
        public const string White = "\x1B[37;1m";
        public const string YellowOnRedBackground = "\x1B[93;41m";
        public const string ResetColor = "\x1B[0m";
    }
}