﻿#region Licence
// Copyright (c) 2016 Crestron EMEA
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
#endregion
using System;

namespace Crestron.EMEA.SimplUnit.Tests
{
    public class FluentAssertationsWannebeTests
    {
        // ReSharper disable ExpressionIsAlwaysNull
        [Fact]
        public void NullShouldBeNull()
        {            
            object x = null;            
            x.ShouldBeNull();
            x.Invoking(e => e.ShouldNotBeNull()).ShoudThrowAnException();
        }
        // ReSharper restore ExpressionIsAlwaysNull

        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        [InlineData(65535)]
        [InlineData(65536)]
        [InlineData(-1)]
        public void ShouldBeForInt(int x)
        {
            x.ShouldBe(x);            
            x.ShouldNotBeNull();
            x.ShouldBeBetween(x-1, x+1);
        }

        [Theory]
        [InlineData("Hello world")]
        [InlineData("")]
        public void ShouldBeForString(string str)
        {
            str.ShouldBe(str);
            str.ShouldNotBeNull();
        }

        [Fact]
        public void ShouldThrowException_Static()
        {
            //normal usage
            DivideSixWith(6).ShouldBe(1);
            
            //Expecting an exception requiers some special constructs
            ((Action)(() => DivideSixWith(0))).ShoudThrowAnException();
            //Or
            Action action = () => DivideSixWith(0);
            action.ShoudThrowAnException();            
        }

        // ReSharper disable once ConvertClosureToMethodGroup
        [Fact]
        public void ShouldThrowException_Instance()
        {
            var foo = new Foo();

            //You can use the same construct as with a static method
            Action act = () => foo.MethodWithException();                        
            act.ShoudThrowAnException();
            
            //Or
            foo.ShoudThrowAnException(e => e.MethodWithException());

            //Or, since we read from left to right
            foo.Invoking(f => f.MethodWithException()).ShoudThrowAnException();
            
            //You can also use Invoking while no exception is expected
            foo.Invoking(f => f.DivideFiveWith(0)).ShoudThrowAnException();
            foo.Invoking(f => f.DivideFiveWith(5)).ShouldBe(1);
            //The last one is the same as
            foo.DivideFiveWith(5).ShouldBe(1);
        }

        private static int DivideSixWith(int x)
        {
            return 6/x;            
        }

        private class Foo
        {
            public void MethodWithException()
            {
                throw new Exception("You should not do this");                
            }

            public int DivideFiveWith(int x)
            {
                return 5 / x;
            }
        }        
    }
}