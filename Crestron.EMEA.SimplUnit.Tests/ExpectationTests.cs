﻿using Crestron.EMEA.SimplUnit.SimplMock;
using Crestron.EMEA.SimplUnit.Tests.SimpleMock.Mocks;

namespace Crestron.EMEA.SimplUnit.Tests
{
    public class ExpectationTests
    {
        [Fact]
        public void EvaluateSignature_1()
        {
            var mockInvoker = new MockInvoker();
            var sut = new ExpectationVoid(mockInvoker, "Foo", 1, 2);

            sut.EvaluateSignature("Foo", 1, 2).ShouldBe(true);
            sut.EvaluateSignature("Foo", 1, 1).ShouldBe(false);
            sut.EvaluateSignature("Fo", 1, 2).ShouldBe(false);
            sut.EvaluateSignature("Foo", 1, 2, 3).ShouldBe(false);
        }

        [Fact]
        public void EvaluateSignature_2()
        {
            var mockInvoker = new MockInvoker();
            var sut = new ExpectationVoid(mockInvoker, "Foo");

            sut.EvaluateSignature("Foo").ShouldBe(true);
            sut.EvaluateSignature("Foo", null).ShouldBe(false);
            sut.EvaluateSignature("Fo").ShouldBe(false);
        }

        [Fact]
        public void EvaluateSignature_3()
        {
            var mockInvoker = new MockInvoker();
            var sut = new ExpectationVoid(mockInvoker, "Foo", new Arg<int>[] { });

            sut.EvaluateSignature("Foo", new Arg<int>[] { }).ShouldBe(true);
            sut.EvaluateSignature("Foo", new Arg<string>[] { }).ShouldBe(false);
            sut.EvaluateSignature("Foo", null).ShouldBe(false);
            sut.EvaluateSignature("Foo").ShouldBe(false);
        }

        [Fact]
        public void EvaluateSignature_4()
        {
            var mockInvoker = new MockInvoker();
            var sut = new ExpectationVoid(mockInvoker, "Foo", new int[] { });

            sut.EvaluateSignature("Foo", new int[] { }).ShouldBe(true);
            sut.EvaluateSignature("Foo", new string[] { }).ShouldBe(false);
            sut.EvaluateSignature("Foo", null).ShouldBe(false);
            sut.EvaluateSignature("Foo").ShouldBe(false);
        }
    }
}