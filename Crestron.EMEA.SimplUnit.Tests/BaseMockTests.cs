﻿using Crestron.EMEA.SimplUnit.Tests.Example.Mocks;

namespace Crestron.EMEA.SimplUnit.Tests
{
    //We use SomeMock since BaseMock by definition is not instantiatable
    //Those test are intended to test the BaseMock implementation
    public class BaseMockTests
    {        
        [Fact]
        public void VerifyAllExpectationsShouldSucceedByDefault()
        {
            var sutSomeMock = new SomeMock();
            sutSomeMock.VerifyAllExpectations();
        }

        [Fact]
        public void VerifyAllExpectations_Once()
        {
            var sutSomeMock = new SomeMock();            
            sutSomeMock.Expect(e => e.DoStuff()).Return("").Once();

            sutSomeMock.DoStuff();

            sutSomeMock.VerifyAllExpectations();
        }

        [Fact]
        public void VerifyAllExpectations_Once_Failed()
        {
            var sutSomeMock = new SomeMock();
            sutSomeMock.Expect(e => e.DoStuff()).Return("").Once();       

            //We don't call DoStuff()
            
            sutSomeMock.Invoking(e => e.VerifyAllExpectations()).ShoudThrowAnException();
        }

        [Fact]
        public void VerifyAllExpectations_Action_Once()
        {
            var sutSomeMock = new SomeMock();
            sutSomeMock.Expect(e => e.DoVoid()).Once();

            sutSomeMock.DoVoid();

            sutSomeMock.VerifyAllExpectations();
        }

        [Fact]
        public void VerifyAllExpectations_Action_Once_Failed()
        {
            var sutSomeMock = new SomeMock();
            sutSomeMock.Expect(e => e.DoVoid()).Once();

            //We don't call DoVoid()

            sutSomeMock.Invoking(e => e.VerifyAllExpectations()).ShoudThrowAnException();
        }
    }
}