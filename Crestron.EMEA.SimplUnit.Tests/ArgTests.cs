﻿using System.Linq;
using Crestron.EMEA.SimplUnit.Tests.Example;
using Crestron.EMEA.SimplUnit.Tests.Example.Mocks;
using Crestron.EMEA.SimplUnit.Tools;

namespace Crestron.EMEA.SimplUnit.Tests
{
    // ReSharper disable SuspiciousTypeConversion.Global
    public class ArgTests
    {
        [Fact]
        public void ArgShouldBeEqualInt()
        {
            Arg<int> sut = 5;

            sut.Equals(5).ShouldBe(true);
            sut.Equals(7).ShouldBe(false);

            5.Equals(sut).ShouldBe(true);
            7.Equals(sut).ShouldBe(false);
        }

        [Fact]
        public void ArgAnyShouldBeEqualInt()
        {
            var sut = Arg<int>.Any;

            sut.Equals(5).ShouldBe(true);
            sut.Equals(7).ShouldBe(true);

            //This is not 'by design' it is just that we can't change the Equals opperation of the default classes.
            5.Invoking(i => i.Equals(sut)).ShoudThrowAnException();
            7.Invoking(i => i.Equals(sut)).ShoudThrowAnException();
        }

        [Fact]
        public void ArgShouldBeEqualString()
        {
            Arg<string> sut = "Hello World";

            sut.Equals("Hello World").ShouldBe(true);
            sut.Equals("").ShouldBe(false);

            "Hello World".Equals(sut).ShouldBe(true);
            "".Equals(sut).ShouldBe(false);
        }

        [Fact]
        public void ArgShouldBeEqualSomeClass()
        {
            Arg<SomeInterface> sut1 = new SomeMock();
            Arg<SomeMock> sut2 = new SomeMock();
            SomeInterface sut3 = new SomeMock();

            sut1.ShouldNotBeNull();
            sut2.ShouldNotBeNull();
            sut3.ShouldNotBeNull();

            //Not possible to do:
            //Arg<SomeInterface> sut4 = sut1.Value;
            //Arg<SomeInterface> sut5 = (SomeInterface)new SomeMock();
        }

        [Fact]
        public void ArgAnyShouldBeEqualString()
        {
            var sut = Arg<string>.Any;

            sut.Equals("Hello World").ShouldBe(true);
            sut.Equals("").ShouldBe(true);

            // please note that you should always compare Arg with someValue
            // not the other way around
            "".Invoking(e => e.Equals(sut)).ShoudThrowAnException();
        }


        [Fact]
        public void ArgAnyShouldBeEqualNull()
        {
            Arg<string>.Any.Equals(null).ShouldBe(true);                  
        }

        [Fact]
        public void ArgsShouldBeComparable()
        {
            var sut1 = new Arg<int>(42);
            var sut2 = new Arg<int>(42);

            sut1.Equals(sut2).ShouldBe(true);
            sut2.Equals(sut1).ShouldBe(true);

        }
    
        [Fact]
        public void EmptyArgArraysShouldBeEqual()
        {
            var sut1 = new Arg<object>[] { };
            var sut2 = new Arg<object>[] { };

            sut1.SequenceEqual(sut2).ShouldBe(true);
            //Leaving this as a reminder
            sut1.Equals(sut2).ShouldBe(false);
        }

        [Fact]
        public void ArgWithEmptyArgArraysShouldBeEqual()
        {
            var sut1 = new Arg<object>[] { new Arg<object>[] { } };
            var sut2 = new Arg<object>[] { new Arg<object>[] { } };

            // ReSharper disable once RedundantTypeArgumentsOfMethod            
            sut1.SequenceEqual<Arg<object>>(sut2, new ArgComparer<object>()).ShouldBe(true);

            //Leaving this as a reminder
            sut1.SequenceEqual(sut2).ShouldBe(true);
            sut1.Equals(sut2).ShouldBe(false);
        }

        
        [Fact]
        public void ArgWithEmptyArgsRecursiveArraysShouldBeEqual()
        {
            var sut1 = new Arg<object>[] { new Arg<object>[] { new Arg<object>[] { } } };
            var sut2 = new Arg<object>[] { new Arg<object>[] { new Arg<object>[] { } } };

            // ReSharper disable once RedundantTypeArgumentsOfMethod            
            sut1.SequenceEqual<Arg<object>>(sut2, new ArgComparer<object>()).ShouldBe(true);

            //Leaving this as a reminder
            sut1.SequenceEqual(sut2).ShouldBe(true);
            sut1.Equals(sut2).ShouldBe(false);
        }
        // ReSharper restore SuspiciousTypeConversion.Global
    }
}