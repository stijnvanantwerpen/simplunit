﻿namespace Crestron.EMEA.SimplUnit.Tests
{
    public class ArgMatchTests
    {
        // ReSharper disable SuspiciousTypeConversion.Global
        [Fact]
        public void ArgMatch_ShouldBeEvaluated()
        {
            var sut = Arg<string>.Match(s => s.Length == 5);

            sut.Equals("Hello").ShouldBe(true);
            sut.Equals("World!").ShouldBe(false);
        }
        // ReSharper restore SuspiciousTypeConversion.Global
    }
}