﻿using Crestron.EMEA.SimplUnit.Tests.Example.Mocks;

namespace Crestron.EMEA.SimplUnit.Tests
{
    public class StrictTests
    {
        [Fact]
        public void DefaultShouldBeNoneStrict()
        {
            var someMock = new SomeMock();

            //no expectations set, assume none strict call
            someMock.DoVoid(); 
            someMock.DoStuff();
            someMock.DoStuffWith("some value");

            someMock.VerifyAllExpectations();
        }

        [Fact]
        public void StrictModeShouldNotAllowUnsetExpectations()
        {
            var someMock = new SomeMock();            
            someMock.SetStrict();
            
            someMock.ShoudThrowAnException(mock => mock.DoVoid());            
        }
    }
}