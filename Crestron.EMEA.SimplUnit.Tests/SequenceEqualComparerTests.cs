﻿using System.Linq;
using Crestron.EMEA.SimplUnit.Tools;

namespace Crestron.EMEA.SimplUnit.Tests
{
    public class SequenceEqualComparerTests
    {
        [Fact]
        public void ShouldCompareListOfInt()
        {
            var arr1 = new [] {1, 2, 3, 5};
            var arr2 = new[] { 1, 2, 3, 5 };
            var arr3 = new[] { 1, 2, 3 };

            var sut = new SequenceEqualComparer();

            var hash1 = sut.GetHashCode(arr1);
            var hash2 = sut.GetHashCode(arr2);
            var hash3 = sut.GetHashCode(arr3);
            var areEqual1And2 = sut.Equals(arr1, arr2);
            var areEqual1And3 = sut.Equals(arr1, arr3);

            areEqual1And2.ShouldBe(true);
            areEqual1And3.ShouldBe(false);
            hash1.ShouldBe(hash2);
            hash1.ShouldNotBe(hash3);
        }

        [Fact]
        public void ShouldCompareListOfString()
        {
            var arr1 = new[] { "xyz", "a", "b", "c" };
            var arr2 = new[] { "xyz", "a", "b", "c" };
            var arr3 = new[] { "xyz", "a", "b", "d" };

            var sut = new SequenceEqualComparer();

            var hash1 = sut.GetHashCode(arr1);
            var hash2 = sut.GetHashCode(arr2);
            var hash3 = sut.GetHashCode(arr3);
            var areEqual1And2 = sut.Equals(arr1, arr2);
            var areEqual1And3 = sut.Equals(arr1, arr3);

            areEqual1And2.ShouldBe(true);
            areEqual1And3.ShouldBe(false);
            hash1.ShouldBe(hash2);
            hash1.ShouldNotBe(hash3);
        }

        [Fact]
        public void ShouldCompareListOfMixed()
        {
            var arr1 = new object[] { "xyz", "a", 1 , "c" };
            var arr2 = new object[] { "xyz", "a", 1 , "c" };
            var arr3 = new object[] { "xyz", "a", 2, "d" };

            var sut = new SequenceEqualComparer();

            var hash1 = sut.GetHashCode(arr1);
            var hash2 = sut.GetHashCode(arr2);
            var hash3 = sut.GetHashCode(arr3);
            var areEqual1And2 = sut.Equals(arr1, arr2);
            var areEqual1And3 = sut.Equals(arr1, arr3);

            areEqual1And2.ShouldBe(true);
            areEqual1And3.ShouldBe(false);
            hash1.ShouldBe(hash2);
            hash1.ShouldNotBe(hash3);
        }

        [Fact]
        public void ShouldCompareListOfMixedRecursisive()
        {
            var arr1 = new object[] { "xyz", "a", new object[] { 1, 2, "hello world" }, "c" };
            var arr2 = new object[] { "xyz", "a", new object[] { 1, 2, "hello world" }, "c" };
            var arr3 = new object[] { "xyz", "a", new object[] { 1, 2, "hello *****" }, "c" };

            var sut = new SequenceEqualComparer();

            var hash1 = sut.GetHashCode(arr1);
            var hash2 = sut.GetHashCode(arr2);
            var hash3 = sut.GetHashCode(arr3);
            var areEqual1And2 = sut.Equals(arr1, arr2);
            var areEqual1And3 = sut.Equals(arr1, arr3);

            areEqual1And2.ShouldBe(true);
            areEqual1And3.ShouldBe(false);
            hash1.ShouldBe(hash2);
            hash1.ShouldNotBe(hash3);
        }

        [Fact]
        public void ShouldCompareEmptyLists()
        {
            var arr1 = new object[] { };
            var arr2 = new object[] { };

            var sut = new SequenceEqualComparer();
            var areEquel1And2 = sut.Equals(arr1, arr2);

            areEquel1And2.ShouldBe(true);
        }

        [Fact]
        public void ShouldCompareEmptyLists_int()
        {
            var arr1 = new int[] { };
            var arr2 = new int[] { };

            var sut = new SequenceEqualComparer();
            var areEquel1And2 = sut.Equals(arr1, arr2);

            areEquel1And2.ShouldBe(true);
        }

        [Fact]
        public void MakesADifferance()
        {
            var arr1 = new object[] { new object[]{} };
            var arr2 = new object[] { new object[]{} };

            var sut = new SequenceEqualComparer();

            var with = arr1.SequenceEqual(arr2, sut);
            var withOut = arr1.SequenceEqual(arr2);

            with.ShouldBe(true);
            withOut.ShouldBe(false);
        }

        [Fact]
        public void MakesADifferance_Recursive()
        {
            var arr1 = new object[] { new object[] { new object[]{} } };
            var arr2 = new object[] { new object[] { new object[]{} } };

            var sut = new SequenceEqualComparer();

            var with = arr1.SequenceEqual(arr2, sut);
            var withOut = arr1.SequenceEqual(arr2);

            with.ShouldBe(true);
            withOut.ShouldBe(false);
        }

        [Fact]
        public void MakesADifferance_Recursive_StillSeeingADifferance()
        {
            var arr1 = new object[] { new object[] { new object[] { } } };
            var arr2 = new object[] { new object[] { new int[] { } } };

            var sut = new SequenceEqualComparer();

            var with = arr1.SequenceEqual(arr2, sut);
            var withOut = arr1.SequenceEqual(arr2);

            with.ShouldBe(true);
            withOut.ShouldBe(false);
        }
    }
}