#region Licence
// Copyright (c) 2016 Crestron EMEA
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;
using Crestron.EMEA.SimplUnit.SimplMock;

namespace Crestron.EMEA.SimplUnit.Tests.Example.Mocks
{
    // Example: You need to create a Mock for SomeInterface.
    //
    // First create a Invoker for the Type (See details in SomeInvoker)
    //
    // Your Mock should inherent from BaseMock<TInvoker> and implement the Interface you are mocking
    // 
    // You should provide a new Invoker in the constructor
    //
    // Implement each method in the interface with
    //     return Do(i => i.YourMethodCallToTheInvoker());  
    // When the method is a void, just ommit the return.
    //
    //
    // Feel free to write a code generator for it.
    //
    // NOTE: The "real" mockingframeworks uses either DynamicObjects or System.Reflection.Emit to 
    //       generate mock instances at runtime. Unfortunaltly none of those options are available
    //       in .NET compact Framework. This is propebly the least possible effort you have to do 
    //       to create a mock object.
    public class SomeMock : BaseMock<SomeInvoker> , SomeInterface
    {
        public delegate void MyDelegate(int x, string y);

        public event EventHandler<SomeEventArgs> SomeEvent;
        public event EventHandler SomeEventWithoutEventArgs;
        public event MyDelegate SomeEventFromDelegate;

        public string DoStuff()
        {
            return Do(i => i.DoStuff());
        }

        public string DoOtherStuff()
        {
            return Do(i => i.DoOtherStuff());
        }

        public int DoIntStuff()
        {
            return Do(i => i.DoIntStuff());
        }

        public string DoStuffWith(int p)
        {
            return Do(i => i.DoStuffWith(p));
        }

        public string DoStuffWith(string p)
        {
            return Do(i => i.DoStuffWith(p));
        }

        public void DoVoid()
        {
            Do(i => i.DoVoid());
        }

        public Action<T> GetAction<T>()
        {
            return Do(i => i.GetAction<T>());
        }

        public EventInvoker<SomeEventArgs> GetSomeEvent()
        {
            return GetEvent(SomeEvent);
        }

        public EventInvoker GetSomeEventWithoutEventArgs()
        {
            return GetEvent(SomeEventWithoutEventArgs);
        }

        public EventInvoker<int, string> GetSomeEventFromDelegate()
        {
            return GetEvent<int, string>((arg1, arg2) => SomeEventFromDelegate(arg1, arg2));
        }
    }
}