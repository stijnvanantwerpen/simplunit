#region Licence
// Copyright (c) 2016 Crestron EMEA
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;
using Crestron.EMEA.SimplUnit.SimplMock;

namespace Crestron.EMEA.SimplUnit.Tests.Example.Mocks
{
    // Example: You need to create a Invoker for SomeInterface.
    //
    // Create a method for each one in the interface , with 
    //     Expectation<ReturnType> as the return type
    //     Arg<T> as the type of each paramter, where T is the actual type
    //
    // Implement each function with:
    //      return GetExpectationFor<ReturnType>("MethodName");
    // Or
    //      return GetExpectationFor<ReturnType>("MethodName", par1, par2);
    //
    // Feel free to write a code generator for it.
    //
    public class SomeInvoker : Invoker
    {
        public Expectation<string> DoStuff()
        {
            return GetExpectationFor<string>("DoStuff");           
        }

        public Expectation<string> DoOtherStuff()
        {
            return GetExpectationFor<string>("DoOtherStuff");
        }

        public Expectation<int> DoIntStuff()
        {
            return GetExpectationFor<int>("DoIntStuff");
        }

        public Expectation<string> DoStuffWith(Arg<int> arg)
        {
            return GetExpectationFor<string>("DoStuffWith", arg); 
        }

        public Expectation<string> DoStuffWith(Arg<string> arg)
        {
            return GetExpectationFor<string>("DoStuffWith", arg);
        }

        public ExpectationVoid DoVoid()
        {
            return GetExpectationFor("DoVoid");
        }

        public Expectation<Action<T>> GetAction<T>()
        {
            var methodName = String.Format("Action<{0}>", typeof (T).FullName);
            return GetExpectationFor<Action<T>>(methodName);
        }

        public EventInvoker<SomeEventArgs> SomeEvent
        {
            get { return ((SomeMock)Mock).GetSomeEvent(); }
        }

        public EventInvoker SomeEventWithoutEventArgs
        {
            get { return ((SomeMock)Mock).GetSomeEventWithoutEventArgs(); }
        }

        public EventInvoker<int, string> SomeEventFromDelegate
        {
            get { return ((SomeMock)Mock).GetSomeEventFromDelegate(); }
        }
    }
}