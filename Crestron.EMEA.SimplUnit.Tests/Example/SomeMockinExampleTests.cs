﻿// Copyright (c) 2016 Crestron EMEA
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.

using Crestron.EMEA.SimplUnit.Tests.Example.Mocks;

namespace Crestron.EMEA.SimplUnit.Tests.Example
{
    public class SomeMockinExampleTests
    {
        [Fact]
        public void AnExpectationShouldReturnItReturnValue()
        {
            var sut = new SomeMock();

            sut.Expect(e => e.DoStuff()).Return("Hello World");
            sut.Expect(e => e.DoOtherStuff()).Return("Hello Other");
            sut.Expect(e => e.DoIntStuff()).Return(42);                                                

            sut.DoStuff().ShouldBe("Hello World");
            sut.DoOtherStuff().ShouldBe("Hello Other");
            sut.DoIntStuff().ShouldBe(42);
            sut.DoVoid();
        }

        [Fact]
        public void AnExpectationShouldReturnItReturnValueForParameter()
        {
            var sut = new SomeMock();

            sut.Expect(e => e.DoStuffWith(1)).Return("Hello World");
            sut.Expect(e => e.DoStuffWith(2)).Return("Hello Two");
            sut.Expect(e => e.DoStuffWith(3)).Return("Hello Three");

            sut.DoStuffWith(3).ShouldBe("Hello Three");
            sut.DoStuffWith(1).ShouldBe("Hello World");
            sut.DoStuffWith(2).ShouldBe("Hello Two");            
        }

        [Fact]
        public void AnExpectationShouldReturnValueForMultipleCalls()
        {
            var sut = new SomeMock();

            sut.Expect(e => e.DoStuffWith(1)).Return("First");
            sut.Expect(e => e.DoStuffWith(1)).Return("Second");
            sut.Expect(e => e.DoStuffWith(1)).Return("Third");

            sut.DoStuffWith(1).ShouldBe("First");
            sut.DoStuffWith(1).ShouldBe("Second");
            sut.DoStuffWith(1).ShouldBe("Third");            
        }

        [Fact]
        public void AnExpectationShouldBeRepeatable()
        {
            var sut = new SomeMock();

            sut.Expect(e => e.DoStuffWith(0)).Never();
            sut.Expect(e => e.DoStuffWith(1)).Return("First").Always();
            sut.Expect(e => e.DoStuffWith(2)).Return("Second").Once();
            sut.Expect(e => e.DoStuffWith(3)).Return("Third").Repeat(3);
            sut.Expect(e => e.DoStuffWith(3)).Return("Sixt").Once();

            sut.DoStuffWith(1).ShouldBe("First");
            sut.DoStuffWith(1).ShouldBe("First");
            sut.DoStuffWith(1).ShouldBe("First");
            sut.DoStuffWith(1).ShouldBe("First");
            sut.DoStuffWith(2).ShouldBe("Second");
            sut.DoStuffWith(1).ShouldBe("First");
            sut.DoStuffWith(1).ShouldBe("First");
            sut.DoStuffWith(1).ShouldBe("First");
            sut.DoStuffWith(3).ShouldBe("Third");
            sut.DoStuffWith(1).ShouldBe("First");
            sut.DoStuffWith(1).ShouldBe("First");
            sut.DoStuffWith(3).ShouldBe("Third");
            sut.DoStuffWith(1).ShouldBe("First");
            sut.DoStuffWith(3).ShouldBe("Third");
            sut.DoStuffWith(1).ShouldBe("First");
            sut.DoStuffWith(1).ShouldBe("First");
            sut.DoStuffWith(3).ShouldBe("Sixt"); //The fourth time with param 3
            sut.DoStuffWith(1).ShouldBe("First");
            sut.DoStuffWith(1).ShouldBe("First");
            sut.DoStuffWith(2).ShouldBe(default(string));
            sut.DoStuffWith(3).ShouldBe(default(string));
        }

        [Fact]
        public void SupportForAnyArgs()
        {
            var sut = new SomeMock();

            sut.Expect(e => e.DoStuffWith(Arg<int>.Any)).Return("Hello").Always();

            sut.DoStuffWith(1).ShouldBe("Hello");
            sut.DoStuffWith(0).ShouldBe("Hello");
            sut.DoStuffWith(99).ShouldBe("Hello");
            sut.DoStuffWith(514).ShouldBe("Hello");
        }

        [Fact]
        public void AtLeastOnce()
        {
            var sut = new SomeMock();

            sut.Expect(e => e.DoStuff()).Return("Hello World").AtLeastOnce();

            sut.DoStuff().ShouldBe("Hello World");
            sut.DoStuff().ShouldBe("Hello World");
            sut.DoStuff().ShouldBe("Hello World");
            sut.DoStuff().ShouldBe("Hello World");
            
            sut.VerifyAllExpectations();
        }

        [Fact]
        public void MultipleOnce()
        {
            var sut = new SomeMock();

            sut.Expect(e => e.DoStuff()).Return("Hello World 1").Once();
            sut.Expect(e => e.DoStuff()).Return("Hello World 2").Once();
            sut.Expect(e => e.DoStuff()).Return("Hello World 3").Once();
            sut.Expect(e => e.DoStuff()).Return("Hello World 4").Once();

            sut.DoStuff().ShouldBe("Hello World 1");
            sut.DoStuff().ShouldBe("Hello World 2");
            sut.DoStuff().ShouldBe("Hello World 3");
            sut.DoStuff().ShouldBe("Hello World 4");            

            sut.VerifyAllExpectations();
        }

        [Fact]
        public void MultipleOnce_ToMany_DefaultBehavior()
        {            
            var sut = new SomeMock();

            sut.Expect(e => e.DoStuff()).Return("Hello World 1").Once();
            sut.Expect(e => e.DoStuff()).Return("Hello World 2").Once();
            sut.Expect(e => e.DoStuff()).Return("Hello World 3").Once();
            sut.Expect(e => e.DoStuff()).Return("Hello World 4").Once();

            sut.DoStuff().ShouldBe("Hello World 1");
            sut.DoStuff().ShouldBe("Hello World 2");
            sut.DoStuff().ShouldBe("Hello World 3");
            sut.DoStuff().ShouldBe("Hello World 4");
            sut.DoStuff().ShouldBe(default(string));

            sut.VerifyAllExpectations();
        }

        [Fact]
        public void Match_EvenOrOdd()
        {
            var sut = new SomeMock();

            sut.Expect(e => e.DoStuffWith(Arg<int>.Match(x => x % 2 == 0))).Return("even").Always();
            sut.Expect(e => e.DoStuffWith(Arg<int>.Match(x => x % 2 != 0))).Return("odd").Always();

            sut.DoStuffWith(0).ShouldBe("even");
            sut.DoStuffWith(2).ShouldBe("even");
            sut.DoStuffWith(7).ShouldBe("odd");
            sut.DoStuffWith(8).ShouldBe("even");
            sut.DoStuffWith(512).ShouldBe("even");
            sut.DoStuffWith(999).ShouldBe("odd");

            sut.VerifyAllExpectations();
        }
    }
}