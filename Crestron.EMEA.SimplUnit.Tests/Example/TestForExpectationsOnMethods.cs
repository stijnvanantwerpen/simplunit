﻿using System;
using Crestron.EMEA.SimplUnit.Tests.Example.Mocks;

namespace Crestron.EMEA.SimplUnit.Tests.Example
{

    public class CompleExpectationTests
    {
        [Fact]
        public void ShouldWorkForReturnTypeActionT()
        {
            var mock = new SomeMock();
            mock.Expect(e => e.GetAction<int>()).Return(x => { if (x != 1) throw new Exception("x should be 1"); });
            mock.Expect(e => e.GetAction<int>()).Return(x => { if (x != 2) throw new Exception("x should be 2"); });
            mock.Expect(e => e.GetAction<string>())
                .Return(x => { if (x != "hello world") throw new Exception("x should be 'hello world'"); });

            mock.GetAction<int>()(1);
            mock.GetAction<int>()(2);
            mock.GetAction<string>()("hello world");
        }

        [Fact]
        public void ShouldWorkForReturnTypeActionT_OtherSequence()
        {
            var mock = new SomeMock();
            mock.Expect(e => e.GetAction<int>()).Return(x => { if (x != 1) throw new Exception("x should be 1"); });
            mock.Expect(e => e.GetAction<int>()).Return(x => { if (x != 2) throw new Exception("x should be 2"); });
            mock.Expect(e => e.GetAction<string>())
                .Return(x => { if (x != "hello world") throw new Exception("x should be 'hello world'"); });

            mock.GetAction<string>()("hello world");
            mock.GetAction<int>()(1);
            mock.ShoudThrowAnException(m => m.GetAction<int>()(1));
        }
    }
}