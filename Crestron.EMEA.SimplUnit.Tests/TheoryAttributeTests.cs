﻿// Copyright (c) 2016 Crestron EMEA
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.

using System;

namespace Crestron.EMEA.SimplUnit.Tests
{
    public class TheoryAttributeTests
    {
        [Theory]
        [InlineData(1, 2, 3)]
        public void TestInlineDataOnTheory(int x, int y, int z)
        {
            //This is a test for the Inline data attribute 
            x.ShouldBe(1);
            y.ShouldBe(2);
            z.ShouldBe(3);
        }

        [Theory]
        [InlineData(1, "SomeString", true)]
        public void TestInlineDataOnTheoryWithDifferentTypes(int x, string y, bool z)
        {
            //This is a test for the Inline data attribute 
            x.ShouldBe(1);
            y.ShouldBe("SomeString");
            z.ShouldBe(true);
        }

        [Theory]
        [InlineData(1, 2, new []{3,4,5} )]
        public void TestInlineDataOnTheoryWithArray(int x, int y, int[] arr)
        {
            //This is a test for the Inline data attribute 
            x.ShouldBe(1);
            y.ShouldBe(2);
            arr[0].ShouldBe(3);
            arr[1].ShouldBe(4);
            arr[2].ShouldBe(5);
        }

        [Theory]
        [InlineData(1, 2, 3)]
        [InlineData(4, 5, 6)]
        [InlineData(0, 0, 0, Skip = "Test for skip of Inline data")]        
        public void TestInlineDataOnTheoryMultiple(int x, int y, int z)
        {
            //This is a test for the Inline data attribute 
            x.ShouldNotBe(0);

            if (x == 1)
            {
                y.ShouldBe(2);
                z.ShouldBe(3);
            }
            else
            {
                x.ShouldBe(4);
                y.ShouldBe(5);
                z.ShouldBe(6);
            }            
        }       

        [Theory]
        [InlineData("?543210", 'F', true, 6, '?', "543210", (Byte)0x87)]
        public void FieldProblem1_solution(
            string tx,
            char expectedHeader,
            bool expectedAckRequiered,
            int expectedLength,
            char expectedBlockCode,
            string expectedBody,
            byte expectedCrc)
        {
            tx.ShouldNotBeNull();
            expectedAckRequiered.ShouldNotBeNull();
            expectedAckRequiered.ShouldNotBeNull();
            expectedLength.ShouldNotBeNull();
            expectedBlockCode.ShouldNotBeNull();
            expectedBody.ShouldNotBeNull();
            expectedCrc.ShouldNotBeNull();
        }
    }
}

