﻿using Crestron.EMEA.SimplUnit.Tests.Example.Mocks;

namespace Crestron.EMEA.SimplUnit.Tests
{
    public class InvokerTests
    {
        [Fact]
        public void TwoParameterlessMethodesShouldReturnTheSameExpectation()
        {
            var sut = new SomeInvoker();
            var expectation1 = sut.DoStuff();
            var expectation2 = sut.DoStuff();

            expectation1.Equals(expectation2).ShouldBe(true);
        }
    }
}