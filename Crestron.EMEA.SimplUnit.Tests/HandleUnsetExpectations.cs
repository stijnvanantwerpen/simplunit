﻿using Crestron.EMEA.SimplUnit.Tests.Example.Mocks;

namespace Crestron.EMEA.SimplUnit.Tests
{
    public class HandleUnsetExpectations
    {
        [Fact]
        public void AnUnsetVoidMethodShouldBeCallable()
        {
            var someMock = new SomeMock();

            var result = someMock.DoStuff();

            result.ShouldBeNull();
        }

        [Fact]
        public void WhenNeverHasBeenSetItShouldStillFail()
        {
            var someMock = new SomeMock();
            someMock.Expect(e => e.DoStuff()).Never();

            var result = someMock.DoStuff(); //This is fine, for now          
            result.ShouldBe(default(string));

            someMock.ShoudThrowAnException(mock => mock.VerifyAllExpectations());
        }

        [Fact]
        public void WhenNeverHasBeenSetItShouldVerifyWhenNotCalled()
        {
            var someMock = new SomeMock();

            someMock.Expect(e => e.DoStuff()).Never();

            someMock.VerifyAllExpectations();
        } 

        [Fact]
        public void InStrictModeTheSecondCallForExpectedOnceShouldFail()
        {
            var someMock = new SomeMock();
            someMock.SetStrict();
            
            someMock.Expect(e => e.DoStuff()).Once();

            someMock.DoStuff(); //This is fine            
            //this is not
            someMock.ShoudThrowAnException(mock => mock.DoStuff());             
        }

        [Fact]
        public void MultipleExpectationsForSimularCall()
        {
            var someMock = new SomeMock();

            someMock.Expect(e => e.DoStuff()).Return("hello world").Once();
            someMock.Expect(e => e.DoStuff()).Return("hello other").Always();

            var result1 = someMock.DoStuff();
            var result2 = someMock.DoStuff();

            result1.ShouldBe("hello world");
            result2.ShouldBe("hello other");

            someMock.VerifyAllExpectations();
        }

        [Fact]
        public void MultipleExpectationsForSimularCall_Alternative()
        {
            var someMock = new SomeMock();

            someMock.Expect(e => e.DoStuff()).Return("hello world").Once();
            someMock.Expect(e => e.DoStuff()).Always();

            var result1 = someMock.DoStuff();
            var result2 = someMock.DoStuff();

            result1.ShouldBe("hello world");
            result2.ShouldBe(default(string));

            someMock.VerifyAllExpectations();
        }


        [Fact]
        public void WhenAtLeastOnceHasBeenSetItShouldNotFail()
        {
            var someMock = new SomeMock();

            someMock.Expect(e => e.DoStuff()).AtLeastOnce();

            someMock.DoStuff();
            someMock.DoStuff();
            someMock.DoStuff();
            someMock.DoStuff();
            
            someMock.VerifyAllExpectations();
        }

        [Fact]
        public void FieldCase_MatchWithOnce()
        {
            var someMock = new SomeMock();
            someMock.Expect(e => e.DoStuffWith(Arg<string>.Match(arg => arg[2] == ':'))).Once();

            someMock.DoVoid(); //no expectations set
            someMock.DoStuffWith("00:00;"); //Should trigger the Match
            
            someMock.VerifyAllExpectations();
        }

        [Fact]
        public void FieldCase_MatchWithOnce_MoreDetailed()
        {
            var someMock = new SomeMock();            

            someMock.DoVoid(); //no expectations set            

            someMock.VerifyAllExpectations();
        }

       
    }
}