﻿using Crestron.EMEA.SimplUnit.SimplMock;

namespace Crestron.EMEA.SimplUnit.Tests.SimpleMock.Mocks
{
    public class MockInvoker : BaseMock<MockInvokerInvoker>, IInvoker
    {
        public object Mock
        {
            get { return Do(i => i.Mock); }
            set { Do(i => i.Mock.AddOnce(value)); }
        }
    }

    public class MockInvokerInvoker : Invoker
    {
        new public Expectation<object> Mock
        {
            get { return GetExpectationFor<object>("Mock"); }
        }
    }
}