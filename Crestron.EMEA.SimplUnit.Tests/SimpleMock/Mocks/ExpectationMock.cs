﻿using Crestron.EMEA.SimplUnit.SimplMock;

namespace Crestron.EMEA.SimplUnit.Tests.SimpleMock.Mocks
{
    public class ExpectationMock<T> :BaseMock<ExpectationMockInvoker<T>>, Expectation<T>
    {
        public bool EvaluateSignature(string methodeName, params Arg<object>[] args)
        {
            return Do(i => i.EvaluateSignature(methodeName, args));
        }

        public void Verify()
        {
            Do(i => i.Verify());
        }

        public void AddAlways()
        {
            Do(i => i.AddAlways());
        }

        public void AddOnce()
        {
            Do(i => i.AddOnce());
        }

        public string MethodeName
        {
            get { return Do(i => i.MethodeName); }
        }

        public string Description
        {
            get { return Do(i => i.Description); }
        }

        public Expectation<T> AddOnce(T result)
        {
            return Do(i => i.AddOnce(result));
        }

        public T GetResult(bool strict)
        {
            return Do(i => i.GetResult(strict));
        }

        public ExpectationBuilder<T> GetBuilder()
        {
            return Do(i => i.GetBuilder());
        }

        public void AddAlways(T result)
        {
            Do(i => i.AddAlways(result));
        }

        public void AddNever(T result)
        {
            Do(i => i.AddNever(result));
        }
    }

    public class ExpectationMock : BaseMock<ExpectationMockInvoker>, Expectation
    {
        public bool EvaluateSignature(string methodeName, params Arg<object>[] args)
        {
            return Do(i => i.EvaluateSignature(methodeName, args));
        }

        public void Verify()
        {
            Do(i => i.Verify());
        }

        public void AddAlways()
        {
            Do(i => i.AddAlways());
        }

        public void AddOnce()
        {
            Do(i => i.AddOnce());
        }

        public string MethodeName
        {
            get { return Do(i => i.MethodeName); }
        }

        public string Description
        {
            get { return Do(i => i.Description); }
        }
    }

    public class ExpectationMockInvoker<T> : ExpectationMockInvoker
    {
        public Expectation<Expectation<T>> AddOnce(Arg<T> result)
        {
            return GetExpectationFor<Expectation<T>>("AddOnce", result);
        }

        public Expectation<T> GetResult(Arg<bool> strict)
        {
            return GetExpectationFor<T>("GetResult", strict);
        }

        public Expectation<ExpectationBuilder<T>> GetBuilder()
        {
            return GetExpectationFor<ExpectationBuilder<T>>("GetBuilder");
        }

        public ExpectationVoid AddAlways(Arg<T> result)
        {
            return GetExpectationFor("AddAlways", result);
        }

        public ExpectationVoid AddNever(Arg<T> result)
        {
            return GetExpectationFor("AddNever", result);
        }

       
    }
    public class ExpectationMockInvoker: Invoker
    {

    public Expectation<bool> EvaluateSignature(Arg<string> methodeName, Arg<Arg<object>[]> args)
        {
            return GetExpectationFor<bool>("EvaluateSignature", methodeName, args);
        }

        public ExpectationVoid Verify()
        {
            return GetExpectationFor("Verify");
        }

        public ExpectationVoid AddAlways()
        {
            return GetExpectationFor("AddAlways");
        }

        public ExpectationVoid AddOnce()
        {
            return GetExpectationFor("AddOnce");
        }

        public Expectation<string> MethodeName
        {
            get { return GetExpectationFor<string>("MethodeName"); }
        }

        public Expectation<string> Description
        {
            get { return GetExpectationFor<string>("Description"); }
        }
    }
}