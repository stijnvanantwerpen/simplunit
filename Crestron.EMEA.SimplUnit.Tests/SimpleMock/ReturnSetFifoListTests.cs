﻿using System;
using Crestron.EMEA.SimplUnit.SimplMock;
using Crestron.EMEA.SimplUnit.Tests.SdkTessts.Mocks;
using Crestron.EMEA.SimplUnit.Tests.SimpleMock.Mocks;

namespace Crestron.EMEA.SimplUnit.Tests.SimpleMock
{
    public class ReturnSetFifoListTests
    {
        [Fact]
        public void AddAndGetAReturnSet()
        {
            var expectation = new ExpectationMock();
            var resultSet = new ReturnSetOnce(expectation);
            var sut = new ReturnSetFifoList(expectation);

            sut.Add(resultSet);
            var result = sut.GetNextResultSet();

            result.ShouldNotBeNull();
            result.ShouldBe(resultSet);
        }

        [Fact]
        public void AddAndGetAnUnsetReturnSet()
        {
            var expectation = new ExpectationMock();
            var resultSet = new ReturnSetOnce(expectation);
            var sut = new ReturnSetFifoList(expectation);

            sut.Add(resultSet);
            var result1 = sut.GetNextResultSet();
            var result2 = sut.GetNextResultSet();

            result1.ShouldBe(resultSet);

            result2.ShouldNotBeNull();
            result2.ShouldNotBe(resultSet);
        }

        [Fact]
        public void RepetitiveReturnSet()
        {
            var expectation = new ExpectationMock();
            var resultSet = new ReturnSetAlways(expectation);
            var sut = new ReturnSetFifoList(expectation);

            sut.Add(resultSet);
            var result1 = sut.GetNextResultSet();
            var result2 = sut.GetNextResultSet();
            var result3 = sut.GetNextResultSet();

            result1.ShouldBe(resultSet);
            result2.ShouldBe(resultSet);
            result3.ShouldBe(resultSet);
        }

        [Fact]
        public void RepetitiveReturnSetWithReturnValue()
        {
            var expectation = new ExpectationMock();
            var resultSet = new ReturnSetOnce<String>("Hello World", expectation);            
            var sut = new ReturnSetFifoList(expectation);

            sut.Add(resultSet);
            var result1 = sut.GetNextResultSet<String>();
            var result2 = sut.GetNextResultSet<String>();

            result1.ShouldBe(resultSet);            
            result2.ShouldNotBe(resultSet);
        }

        [Fact]
        public void MixedTest()
        {
            var expectation = new ExpectationMock();
            var resultSet1 = new ReturnSetOnce<String>("Hello World", expectation);
            var resultSet2 = new ReturnSetOnce<int>(42, expectation);
            var resultSet3 = new ReturnSetOnce(expectation);
            var resultSet4 = new ReturnSetOnce(expectation);
            var sut = new ReturnSetFifoList(expectation);

            sut.Add(resultSet1);
            sut.Add(resultSet2);
            sut.Add(resultSet3);
            sut.Add(resultSet4);
            var result1 = sut.GetNextResultSet<String>();
            var result2 = sut.GetNextResultSet<int>();
            var result3 = sut.GetNextResultSet();
            var result4 = sut.GetNextResultSet();
            var result5 = sut.GetNextResultSet();
            var result6 = sut.GetNextResultSet<int>();

            result1.ShouldBe(resultSet1);
            result2.ShouldBe(resultSet2);
            result3.ShouldBe(resultSet3);
            result4.ShouldBe(resultSet4);
            result5.ShouldNotBeNull();
            result6.GetResult(false).ShouldBe(default(int));
        }
    }
}