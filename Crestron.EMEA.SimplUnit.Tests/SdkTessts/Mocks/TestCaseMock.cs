﻿using System;
using Crestron.EMEA.SimplUnit.SDK;
using Crestron.EMEA.SimplUnit.SDK.Imp;
using Crestron.EMEA.SimplUnit.SimplMock;

namespace Crestron.EMEA.SimplUnit.Tests.SdkTessts.Mocks
{
    public class TestCaseMock : BaseMock<TestCaseInvoker> , TestCase
    {
        public TestCaseMock() : base(new TestCaseInvoker())
        {
        }

        public event EventHandler<TestExecutedEventArgs> OnTestExecuted;

        public void Execute()
        {
            Do(i => i.Execute());
        }

        public string Description {
            get { return Do(i => i.Description); }
            set { Do(i => i.Description.AddOnce(value)); } 
        }

        public string Skip {
            get { return Do(i => i.Skip); }
            set { Do(i => i.Skip.AddOnce(value)); }
        }
    }
}