﻿using Crestron.EMEA.SimplUnit.SDK;
using Crestron.EMEA.SimplUnit.SimplMock;

namespace Crestron.EMEA.SimplUnit.Tests.SdkTessts.Mocks
{
    public class TestResultInvoker : Invoker
    {
        public Expectation<TestOutcome> TestOutcome
        {
            get { return GetExpectationFor<TestOutcome>("TestOutcome"); }
        }

        public Expectation<string> Message {
            get { return GetExpectationFor<string>("Message"); }
        }
    }
}