﻿using Crestron.EMEA.SimplUnit.SDK;
using Crestron.EMEA.SimplUnit.SimplMock;

namespace Crestron.EMEA.SimplUnit.Tests.SdkTessts.Mocks
{
    public class TestResultMock : BaseMock<TestResultInvoker>, TestResult
    {
        public TestResultMock() : base(new TestResultInvoker())
        {
        }

        public TestOutcome TestOutcome
        {
            get { return Do(i => i.TestOutcome); }
            set { Do(i => i.TestOutcome.AddOnce(value)); }
        }

        public string Message
        {
            get { return Do(i => i.Message); }
            set { Do(i => i.Message.AddOnce(value)); }
        }
    }
}