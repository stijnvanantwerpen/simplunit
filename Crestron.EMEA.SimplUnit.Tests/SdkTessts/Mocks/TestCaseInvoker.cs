﻿using Crestron.EMEA.SimplUnit.SimplMock;

namespace Crestron.EMEA.SimplUnit.Tests.SdkTessts.Mocks
{
    public class TestCaseInvoker : Invoker
    {
        public ExpectationVoid Execute()
        {
            return GetExpectationFor("Execute");
        }

        public Expectation<string> Skip {
            get { return GetExpectationFor<string>("Skip"); }
        }

        public Expectation<string> Description {
            get { return GetExpectationFor<string>("Description"); }
        }
    }
}