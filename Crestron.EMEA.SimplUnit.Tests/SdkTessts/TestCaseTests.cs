﻿using System;
using Crestron.EMEA.SimplUnit.SDK;
using Crestron.EMEA.SimplUnit.SDK.Imp;

namespace Crestron.EMEA.SimplUnit.Tests.SdkTessts
{
    public class TestCaseTests
    {
        [Fact]
        public void ASuccesfullTest()
        {
            const string nameOfTest = "Description of the test";
            var sut = new TestCaseImp(() =>
            { 
                //This is  sucessfull test
            }, nameOfTest);

            var wasCalled = false;
            sut.OnTestExecuted += (sender, args) =>
            {
                wasCalled = true;
                args.Test.Description.ShouldBe(nameOfTest);
                args.Result.TestOutcome.ShouldBe(TestOutcome.Succes);
            };

            sut.Execute();
            wasCalled.ShouldBe(true);
        }

        [Fact]
        public void AFailedTest()
        {
            const string thisIsAFailedTest = "This is a failed test";
            const string nameOfTest = "Description of the test";


            var sut = new TestCaseImp(() =>
            {
                throw new Exception(thisIsAFailedTest);
            }, nameOfTest);

            var wasCalled = false;
            sut.OnTestExecuted += (sender, args) =>
            {
                wasCalled = true;
                args.Result.TestOutcome.ShouldBe(TestOutcome.Failed);
                args.Result.Message.ShouldBe(thisIsAFailedTest);
            };

            sut.Execute();
            wasCalled.ShouldBe(true);
        }

        [Fact]
        public void ASkippedTest()
        {
            const string reasonToSkip = "Reason to skip this test";
            const string nameOfTest = "Description of the test";

            var sut = new TestCaseImp(() =>
            {
                //This is  sucessfull test
            }, nameOfTest);

            sut.Skip = reasonToSkip;
            
            var wasCalled = false;
            sut.OnTestExecuted += (sender, args) =>
            {
                wasCalled = true;
                args.Result.TestOutcome.ShouldBe(TestOutcome.Skipped);
                args.Result.Message.ShouldBe(reasonToSkip);
            };
            
            sut.Execute();
            wasCalled.ShouldBe(true);
        }
    }
}

