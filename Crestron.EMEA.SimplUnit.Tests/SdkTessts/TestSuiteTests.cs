﻿using Crestron.EMEA.SimplUnit.SDK;
using Crestron.EMEA.SimplUnit.SDK.Imp;
using Crestron.EMEA.SimplUnit.Tests.SdkTessts.Mocks;

namespace Crestron.EMEA.SimplUnit.Tests.SdkTessts
{
    public class TestSuiteTests
    {
        [Fact]
        public void AnEmptyTestSuiteShouldBeSkipped()
        {
            const string description = "Description";
            var sutTestSuite = new TestSuiteImp(description);
            var wasCalled = false;

            sutTestSuite.OnTestExecuted += (sender, args) =>
            {
                wasCalled = true;                
            };
            
            sutTestSuite.Execute();
            wasCalled.ShouldBe(false);            
        }
     
        [Fact]
        public void ATestSuiteCouldContainOtherSuites()
        {
            var testSuite1 = new TestSuiteImp("Test Suite 1");
            var testSuite11 = new TestSuiteImp("Test Suite 1.1");
            var testSuite12 = new TestSuiteImp("Test Suite 1.2");
            var testSuite2 = new TestSuiteImp("Test Suite 2");
            var sutTestSuit = new TestSuiteImp("Test Suite SUT");

            var testMock = new TestCaseMock();
            var testResult = new TestResultMock();

            testSuite11.Add(testMock);
            testSuite12.Add(testMock);
            testSuite2.Add(testMock);

            testSuite1.Add(testSuite11);
            testSuite1.Add(testSuite12);
            sutTestSuit.Add(testSuite1);
            sutTestSuit.Add(testSuite2);

            testResult.Expect(e => e.TestOutcome).Return(TestOutcome.Succes).Always();
            testMock.Expect(e => e.Execute()).Repeat(3);


            sutTestSuit.Execute();            
            testMock.VerifyAllExpectations();
        }
    }
}