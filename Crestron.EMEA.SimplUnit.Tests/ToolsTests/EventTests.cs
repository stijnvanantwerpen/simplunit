﻿using Crestron.EMEA.SimplUnit.Tests.Example;
using Crestron.EMEA.SimplUnit.Tests.Example.Mocks;

namespace Crestron.EMEA.SimplUnit.Tests.ToolsTests
{
    public class EventTests
    {
        [Fact]
        public void AnEventShouldBeInvokableFromTests_WithEventArg()
        {
            var mock = new SomeMock();
            var eventWasTriggerd = false;

            mock.SomeEvent += (sender, args) =>
            {
                eventWasTriggerd = true;
                args.HelloWorldString.ShouldBe("Hello World");
            };
            var e = new SomeEventArgs {HelloWorldString = "Hello World"};

            mock.Trigger(m => m.SomeEvent, e);

            eventWasTriggerd.ShouldBe(true);
        }

        [Fact]
        public void AnEventShouldBeInvokableFromTests_WithoutEventArgs()
        {
            var mock = new SomeMock();
            var eventWasTriggerd = false;

            mock.SomeEventWithoutEventArgs += (sender, args) =>
            {
                eventWasTriggerd = true;                
            };            

            mock.Trigger(m => m.SomeEventWithoutEventArgs);

            eventWasTriggerd.ShouldBe(true);
        }

        [Fact]
        public void OnEventShouldBeInvokableFromTests_FromDelegate()
        {
            var mock = new SomeMock();
            var eventWasTriggerd = false;

            mock.SomeEventFromDelegate += (x ,y) =>
            {
                eventWasTriggerd = true;
                x.ShouldBe(42);
                y.ShouldBe("Hello World");
            };            
            mock.Trigger(m => m.SomeEventFromDelegate, 42, "Hello World");

            eventWasTriggerd.ShouldBe(true);
        }
    }
}