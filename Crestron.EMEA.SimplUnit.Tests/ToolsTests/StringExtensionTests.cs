﻿using Crestron.EMEA.SimplUnit.Tools;

namespace Crestron.EMEA.SimplUnit.Tests.ToolsTests
{
    public class StringExtensionTests
    {
        [Fact]
        public void AShortStringShouldNotBeenTruncated()
        {
            var result = "Hello World!".Truncate(20);
            result.ShouldBe("Hello World!");
        }

        [Fact]
        public void ALongStringShouldBeTruncated()
        {
            var result = "Hello World!".Truncate(10);
            result.ShouldBe("Hello W...");
        }
        [Fact]
        public void ALongStringThatIsNotTooLongShouldNotBeTruncated()
        {
            var result = "Hello World!".Truncate(12);
            result.ShouldNotBe("Hello Wor...");
            result.ShouldBe("Hello World!");            
        }
    }
}