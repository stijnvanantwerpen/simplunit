﻿using System.Collections.Generic;
using Crestron.EMEA.SimplUnit.SDK;
using Crestron.EMEA.SimplUnit.SimplMock;

namespace Crestron.EMEA.SimplUnit.ConsoleRunner.Tests.Mocks
{
    public class TestSuiteInvoker : IInvoker
    {
        public Expectation<TestResult> Execute()
        {
            return GetExpectationFor<TestResult>("Execute");
        }

        public Expectation<string> Description {
            get
            {
                return GetExpectationFor<string>("Description");                
            }
        }

        public ExpectationVoid Add(Arg<Test> test)
        {
            return GetExpectationFor("Add", test);
        }

        public ExpectationVoid Add(Arg<IEnumerable<Test>> tests)
        {
            return GetExpectationFor("Add",tests);
        }
    }
}