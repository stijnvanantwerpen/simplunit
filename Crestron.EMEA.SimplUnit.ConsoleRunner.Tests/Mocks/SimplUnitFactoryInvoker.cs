﻿using System;
using Crestron.EMEA.SimplUnit.SDK;
using Crestron.EMEA.SimplUnit.SimplMock;

namespace Crestron.EMEA.SimplUnit.ConsoleRunner.Tests.Mocks
{
    public class SimplUnitFactoryInvoker : IInvoker
    {
        public Expectation<Runner> CreateRunner()
        {
            return GetExpectationFor<Runner>("CreateRunner");
        }

        public Expectation<TestSuite> CreateTestSuite(Arg<string> description)
        {
            return GetExpectationFor<TestSuite>("CreateTestSuite", description);
        }

        public Expectation<TestCase> CreateTestCase(Arg<Action> action, Arg<string> description)
        {
            return GetExpectationFor<TestCase>("CreateTestCase", action, description);
        }
    }
}