﻿using System;
using Crestron.EMEA.SimplUnit.SDK;
using Crestron.EMEA.SimplUnit.SimplMock;

namespace Crestron.EMEA.SimplUnit.ConsoleRunner.Tests.Mocks
{
    public class SimplUnitFactoryMock : BaseMock<SimplUnitFactoryInvoker>, SimplUnitFactory
    {
        public SimplUnitFactoryMock() : base(new SimplUnitFactoryInvoker())
        {
        }

        public Runner CreateRunner()
        {
            return Do(i => i.CreateRunner());
        }

        public TestSuite CreateTestSuite(string description)
        {            
            return Do(i => i.CreateTestSuite(description));
        }

        public TestCase CreateTestCase(Action action, string description)
        {
            return Do(i => i.CreateTestCase(action, description));
        }
    }
}