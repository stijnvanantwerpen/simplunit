﻿using Crestron.EMEA.SimplUnit.SDK;
using Crestron.EMEA.SimplUnit.SimplMock;

namespace Crestron.EMEA.SimplUnit.ConsoleRunner.Tests.Mocks
{
    public class RunnerInvoker : IInvoker
    {
        public ExpectationVoid Add(Arg<Test> test)
        {
            return GetExpectationFor("Add",test);
        }

        public ExpectationVoid Run()
        {
            return GetExpectationFor("Run");
        }
    }
}