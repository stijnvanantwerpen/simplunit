﻿using System;
using System.Collections.Generic;
using Crestron.EMEA.SimplUnit.SDK;
using Crestron.EMEA.SimplUnit.SDK.Imp;
using Crestron.EMEA.SimplUnit.SimplMock;

namespace Crestron.EMEA.SimplUnit.ConsoleRunner.Tests.Mocks
{
    public class TestSuiteMock : BaseMock<TestSuiteInvoker>, TestSuite
    {
        public TestSuiteMock() : base(new TestSuiteInvoker())
        {
        }

        public event EventHandler<TestExecutedEventArgs> OnTestExecuted;

        public void Execute()
        {
            Do(i => i.Execute());
        }

        public string Description
        {
            get { return Do(i => i.Description); }
            set { Do(i => i.Description.AddOnce(value)); }
        }


        public void Add(Test test)
        {
            Do(i => i.Add(new Arg<Test>(test)));
        }

        public void Add(IEnumerable<Test> tests)
        {
            Do(i => i.Add(new Arg<IEnumerable<Test>>(tests)));
        }

        public event EventHandler<TestSuiteEventArgs> OnExecutionBegin;
        public event EventHandler<TestSuiteEventArgs> OnExecutionEnd;
    }
}