﻿using System;
using Crestron.EMEA.SimplUnit.SDK;
using Crestron.EMEA.SimplUnit.SDK.Imp;
using Crestron.EMEA.SimplUnit.SimplMock;

namespace Crestron.EMEA.SimplUnit.ConsoleRunner.Tests.Mocks
{
    public class RunnerMock : BaseMock<RunnerInvoker>, Runner
    {
        public RunnerMock() : base(new RunnerInvoker())
        {

        }

        public void Add(Test test)
        {
            Do(i => i.Add(new Arg<Test>(test)));
        }

        public void Run()
        {
            Do(i => i.Run());
        }

        public event EventHandler<TestExecutedEventArgs> OnTestExecuted;
    }
}