﻿using Crestron.EMEA.SimplUnit.ConsoleRunner.Tests.Mocks;

namespace Crestron.EMEA.SimplUnit.ConsoleRunner.Tests
{
    public class SimplUnitConsoleRunnerTests
    {
        [Fact]
        public void SimplUnitConsoleRunnerShouldRun()
        {
            var factory = new SimplUnitFactoryMock();
            var mockRunner = new RunnerMock();
            factory.Expect(e => e.CreateRunner()).Return(mockRunner).Once();
            mockRunner.Expect(e => e.Run()).Once();

            var sut = new SimplUnitConsoleRunner(factory);
            sut.Run();

            factory.VerifyAllExpectations();
            mockRunner.VerifyAllExpectations();
        }       
    }
}